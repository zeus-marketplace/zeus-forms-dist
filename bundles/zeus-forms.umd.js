(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common'), require('@angular/forms'), require('@angular/material')) :
    typeof define === 'function' && define.amd ? define('@zeus/forms', ['exports', '@angular/core', '@angular/common', '@angular/forms', '@angular/material'], factory) :
    (global = global || self, factory((global.zeus = global.zeus || {}, global.zeus.forms = {}), global.ng.core, global.ng.common, global.ng.forms, global.ng.material));
}(this, function (exports, core, common, forms, material) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    var modules = [
        material.MatInputModule,
        material.MatSelectModule,
        material.MatButtonModule,
        material.MatOptionModule,
        material.MatCheckboxModule,
        material.MatRadioModule,
        material.MatButtonToggleModule,
        material.MatDatepickerModule,
        material.MatFormFieldModule,
        material.MatCardModule
    ];
    var ZeusMaterialModule = /** @class */ (function () {
        function ZeusMaterialModule() {
        }
        ZeusMaterialModule = __decorate([
            core.NgModule({
                declarations: [],
                imports: __spread([
                    common.CommonModule
                ], modules),
                exports: __spread(modules)
            })
        ], ZeusMaterialModule);
        return ZeusMaterialModule;
    }());

    var FieldComponent = /** @class */ (function () {
        function FieldComponent(fb) {
            this.fb = fb;
            this.multiple = false;
            this.control = this.fb.control({
                item: [this.multiple ? [] : null]
            });
            this.complete = new core.EventEmitter();
        }
        FieldComponent.prototype.ngOnInit = function () {
        };
        __decorate([
            core.Input(),
            __metadata("design:type", Object)
        ], FieldComponent.prototype, "type", void 0);
        __decorate([
            core.Input(),
            __metadata("design:type", Object)
        ], FieldComponent.prototype, "placeholder", void 0);
        __decorate([
            core.Input(),
            __metadata("design:type", Object)
        ], FieldComponent.prototype, "options", void 0);
        __decorate([
            core.Input(),
            __metadata("design:type", Object)
        ], FieldComponent.prototype, "label", void 0);
        __decorate([
            core.Input(),
            __metadata("design:type", Object)
        ], FieldComponent.prototype, "multiple", void 0);
        __decorate([
            core.Input(),
            __metadata("design:type", forms.FormControl)
        ], FieldComponent.prototype, "control", void 0);
        __decorate([
            core.Output(),
            __metadata("design:type", Object)
        ], FieldComponent.prototype, "complete", void 0);
        FieldComponent = __decorate([
            core.Component({
                selector: 'zeus-field',
                template: "<ng-container [ngSwitch]=\"type\">\r\n\r\n\r\n  <ng-container *ngSwitchDefault>\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'textarea'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <textarea matInput [formControl]=\"control\" class=\"zeus-field\" rows=\"8\" [placeholder]=\"placeholder\"></textarea>\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'cpf'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" zeusCPF class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'cnpj'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" zeusCNPJ class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'cep'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" zeusCEP class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'telefone'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" zeusTelefone class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'select'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <mat-select [formControl]=\"control\" class=\"zeus-field\" [class.multiple]=\"multiple\" [multiple]=\"multiple\"\r\n        [placeholder]=\"placeholder\">\r\n        <mat-option *ngFor=\"let option of options\" [value]=\"option.value\">\r\n          {{option.label}}\r\n        </mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'checkbox'\">\r\n    <div class=\"zeus-form-field\">\r\n      <mat-checkbox [formControl]=\"control\" class=\"zeus-checkbox\">\r\n        {{label}}\r\n      </mat-checkbox>\r\n    </div>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'radio'\">\r\n    <mat-label class=\"zeus-label\">{{label}}</mat-label>\r\n    <div class=\"zeus-form-field\">\r\n      <mat-radio-group [formControl]=\"control\" class=\"zeus-radio-group\">\r\n        <mat-radio-button class=\"zeus-radio-input\" *ngFor=\"let option of options\" [value]=\"option.value\">\r\n          {{option.label}}</mat-radio-button>\r\n      </mat-radio-group>\r\n    </div>\r\n\r\n  </ng-container>\r\n\r\n  <ng-container *ngIf=\"control?.errors && control?.touched\">\r\n\r\n    <ng-content select=\".zeus-error.zeus-error-required\" *ngIf=\"control.errors.required\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-invalid\" *ngIf=\"control.errors.pattern\"></ng-content>\r\n\r\n    <ng-content select=\".zeus-error.zeus-error-cpf-digit\" *ngIf=\"control.errors.digit\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-cpf-equal\" *ngIf=\"control.errors.equalDigits\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-cpf-length\" *ngIf=\"control.errors.length\"></ng-content>\r\n\r\n    <ng-content select=\".zeus-error.zeus-error-cnpj-digit\" *ngIf=\"control.errors.digit\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-cnpj-equal\" *ngIf=\"control.errors.equalDigits\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-cnpj-length\" *ngIf=\"control.errors.length\"></ng-content>\r\n\r\n  </ng-container>\r\n\r\n  <ng-content select=\".zeus-hint\"></ng-content>\r\n\r\n\r\n</ng-container>",
                styles: [":host{display:block}:host .zeus-form-field{width:100%}"]
            }),
            __metadata("design:paramtypes", [forms.FormBuilder])
        ], FieldComponent);
        return FieldComponent;
    }());

    var ZeusDocumentValidator = /** @class */ (function () {
        function ZeusDocumentValidator() {
        }
        /**
         * Calcula o dígito verificador do CPF ou CNPJ.
         */
        ZeusDocumentValidator.buildDigit = function (arr) {
            var isCpf = arr.length < ZeusDocumentValidator.cpfLength;
            var digit = arr
                .map(function (val, idx) { return val * ((!isCpf ? idx % 8 : idx) + 2); })
                .reduce(function (total, current) { return total + current; }) % ZeusDocumentValidator.cpfLength;
            if (digit < 2 && isCpf) {
                return 0;
            }
            return ZeusDocumentValidator.cpfLength - digit;
        };
        /**
         * Valida um CPF ou CNPJ de acordo com seu dígito verificador.
         */
        ZeusDocumentValidator.validate = function (c) {
            var document = c.value.replace(/\D/g, '');
            // Verifica o tamanho da string.
            if ([ZeusDocumentValidator.cpfLength, ZeusDocumentValidator.cnpjLength].indexOf(document.length) < 0) {
                return { length: true };
            }
            // Verifica se todos os dígitos são iguais.
            if (/^([0-9])\1*$/.test(document)) {
                return { equalDigits: true };
            }
            // A seguir é realizado o cálculo verificador.
            var documentArr = document.split('').reverse().slice(2);
            documentArr.unshift(ZeusDocumentValidator.buildDigit(documentArr));
            documentArr.unshift(ZeusDocumentValidator.buildDigit(documentArr));
            if (document !== documentArr.reverse().join('')) {
                // Dígito verificador não é válido, resultando em falha.
                return { digit: true };
            }
            return null;
        };
        /**
         * Implementa a interface de um validator.
         */
        ZeusDocumentValidator.prototype.validate = function (c) {
            return ZeusDocumentValidator.validate(c);
        };
        ZeusDocumentValidator.cpfLength = 11;
        ZeusDocumentValidator.cnpjLength = 14;
        return ZeusDocumentValidator;
    }());
    var ZeusDocumentDirective = /** @class */ (function (_super) {
        __extends(ZeusDocumentDirective, _super);
        function ZeusDocumentDirective() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        ZeusDocumentDirective_1 = ZeusDocumentDirective;
        var ZeusDocumentDirective_1;
        ZeusDocumentDirective = ZeusDocumentDirective_1 = __decorate([
            core.Directive({
                selector: '[zeusDocument]',
                providers: [{
                        provide: forms.NG_VALIDATORS,
                        useExisting: ZeusDocumentDirective_1,
                        multi: true
                    }]
            })
        ], ZeusDocumentDirective);
        return ZeusDocumentDirective;
    }(ZeusDocumentValidator));

    var ZeusCPFDirective = /** @class */ (function () {
        function ZeusCPFDirective(input) {
            this.input = input;
        }
        ZeusCPFDirective.prototype.ngOnInit = function () {
            this.formControl.setValidators(ZeusDocumentValidator.validate);
        };
        ZeusCPFDirective.prototype.onInput = function (event) {
            var value = event.target.value.replace(/[^0-9]+/g, '').slice(0, 11);
            value = value.replace(/([0-9]{3})([0-9])/, '$1.$2');
            value = value.replace(/\.([0-9]{3})([0-9])/, '.$1.$2');
            value = value.replace(/([0-9])([0-9]{1,2})$/, '$1-$2');
            this.formControl.setValue(value);
        };
        __decorate([
            core.Input('formControl'),
            __metadata("design:type", forms.AbstractControl)
        ], ZeusCPFDirective.prototype, "formControl", void 0);
        __decorate([
            core.HostListener('input', ['$event']),
            __metadata("design:type", Function),
            __metadata("design:paramtypes", [Object]),
            __metadata("design:returntype", void 0)
        ], ZeusCPFDirective.prototype, "onInput", null);
        ZeusCPFDirective = __decorate([
            core.Directive({
                selector: '[zeusCPF]',
            }),
            __metadata("design:paramtypes", [core.ElementRef])
        ], ZeusCPFDirective);
        return ZeusCPFDirective;
    }());

    var ZeusCNPJDirective = /** @class */ (function () {
        function ZeusCNPJDirective(input) {
            this.input = input;
        }
        ZeusCNPJDirective.prototype.ngOnInit = function () {
            this.formControl.setValidators(ZeusDocumentValidator.validate);
        };
        ZeusCNPJDirective.prototype.onInput = function (event) {
            var value = event.target.value.replace(/[^0-9]+/g, '').slice(0, 14);
            value = value.replace(/^(\d{2})(\d)/, '$1.$2'); //Coloca ponto entre o segundo e o terceiro dígitos
            value = value.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3'); //Coloca ponto entre o quinto e o sexto dígitos
            value = value.replace(/\.(\d{3})(\d)/, '.$1/$2'); //Coloca uma barra entre o oitavo e o nono dígitos
            value = value.replace(/(\d{4})(\d)/, '$1-$2'); //Coloca um hífen depois do bloco de quatro dígitos
            this.formControl.setValue(value);
        };
        __decorate([
            core.Input('formControl'),
            __metadata("design:type", forms.AbstractControl)
        ], ZeusCNPJDirective.prototype, "formControl", void 0);
        __decorate([
            core.HostListener('input', ['$event']),
            __metadata("design:type", Function),
            __metadata("design:paramtypes", [Object]),
            __metadata("design:returntype", void 0)
        ], ZeusCNPJDirective.prototype, "onInput", null);
        ZeusCNPJDirective = __decorate([
            core.Directive({
                selector: '[zeusCNPJ]',
            }),
            __metadata("design:paramtypes", [core.ElementRef])
        ], ZeusCNPJDirective);
        return ZeusCNPJDirective;
    }());

    var ZeusCEPDirective = /** @class */ (function () {
        function ZeusCEPDirective(input, host) {
            this.input = input;
            this.host = host;
        }
        ZeusCEPDirective.prototype.ngOnInit = function () {
            console.log(this.host);
        };
        ZeusCEPDirective.prototype.onInput = function (event) {
            var value = event.target.value.replace(/[^0-9]+/g, '').slice(0, 8);
            this.host.complete.emit({
                cep: value
            });
            value = value.replace(/^(\d{2})(\d)/, '$1.$2'); //Coloca ponto entre o segundo e o terceiro dígitos
            value = value.replace(/(\d)(\d{3})$/, '$1-$2'); //Coloca ponto entre o quinto e o sexto dígitos
            this.formControl.setValue(value);
        };
        __decorate([
            core.Input('formControl'),
            __metadata("design:type", forms.AbstractControl)
        ], ZeusCEPDirective.prototype, "formControl", void 0);
        __decorate([
            core.HostListener('input', ['$event']),
            __metadata("design:type", Function),
            __metadata("design:paramtypes", [Object]),
            __metadata("design:returntype", void 0)
        ], ZeusCEPDirective.prototype, "onInput", null);
        ZeusCEPDirective = __decorate([
            core.Directive({
                selector: '[zeusCEP]',
            }),
            __metadata("design:paramtypes", [core.ElementRef, FieldComponent])
        ], ZeusCEPDirective);
        return ZeusCEPDirective;
    }());

    var ZeusPhoneValidator = /** @class */ (function () {
        function ZeusPhoneValidator() {
        }
        /**
         * Valida um CPF ou CNPJ de acordo com seu dígito verificador.
         */
        ZeusPhoneValidator.validate = function (c) {
            var phone = c.value;
            // Verifica o tamanho da string.
            var lengthArray = [
                ZeusPhoneValidator.phoneLength,
                ZeusPhoneValidator.celphoneLength,
                ZeusPhoneValidator.phoneDDDLength,
                ZeusPhoneValidator.celphoneDDDLength
            ];
            if (lengthArray.indexOf(phone.length) < 0) {
                return { length: true };
            }
            var regexArray = [
                /^\d{4}-\d{4}$/,
                /^\d\s\d{4}-\d{4}$/,
                /^\(\d{2}\)\s\d{4}-\d{4}$/,
                /^\(\d{2}\)\s\d\s\d{4}-\d{4}$/
            ];
            var valid = regexArray.some(function (regex) { return regex.test(phone); });
            if (!valid) {
                // Dígito verificador não é válido, resultando em falha.
                return { digit: true };
            }
            return null;
        };
        /**
         * Implementa a interface de um validator.
         */
        ZeusPhoneValidator.prototype.validate = function (c) {
            return ZeusPhoneValidator.validate(c);
        };
        ZeusPhoneValidator.phoneLength = 9;
        ZeusPhoneValidator.celphoneLength = 11;
        ZeusPhoneValidator.phoneDDDLength = 14;
        ZeusPhoneValidator.celphoneDDDLength = 16;
        return ZeusPhoneValidator;
    }());

    var ZeusTelefoneDirective = /** @class */ (function () {
        function ZeusTelefoneDirective(input, host) {
            this.input = input;
            this.host = host;
        }
        ZeusTelefoneDirective.prototype.ngOnInit = function () {
            this.formControl.setValidators(ZeusPhoneValidator.validate);
        };
        ZeusTelefoneDirective.prototype.onInput = function (event) {
            var value = event.target.value.replace(/[^0-9]+/g, '').slice(0, 11);
            this.host.complete.emit({
                telefone: value
            });
            if (value.length <= 8) {
                value = value.replace(/^(\d{4})(\d)/, '$1-$2');
            }
            else if (value.length <= 9) {
                value = value.replace(/^(\d)(\d)/, '$1 $2');
                value = value.replace(/(\d)(\d{4})$/, '$1-$2');
            }
            else if (value.length <= 10) {
                value = value.replace(/^(\d{2})(\d)/, '($1) $2');
                value = value.replace(/(\d)(\d{4})$/, '$1-$2');
            }
            else {
                value = value.replace(/^(\d{2})(\d)/, '($1) $2');
                value = value.replace(/(\d)(\d{8})$/, '$1 $2');
                value = value.replace(/(\d)(\d{4})$/, '$1-$2');
            }
            this.formControl.setValue(value);
        };
        __decorate([
            core.Input('formControl'),
            __metadata("design:type", forms.AbstractControl)
        ], ZeusTelefoneDirective.prototype, "formControl", void 0);
        __decorate([
            core.HostListener('input', ['$event']),
            __metadata("design:type", Function),
            __metadata("design:paramtypes", [Object]),
            __metadata("design:returntype", void 0)
        ], ZeusTelefoneDirective.prototype, "onInput", null);
        ZeusTelefoneDirective = __decorate([
            core.Directive({
                selector: '[zeusTelefone]',
            }),
            __metadata("design:paramtypes", [core.ElementRef, FieldComponent])
        ], ZeusTelefoneDirective);
        return ZeusTelefoneDirective;
    }());

    var ZeusFormModule = /** @class */ (function () {
        function ZeusFormModule() {
        }
        ZeusFormModule = __decorate([
            core.NgModule({
                declarations: [
                    FieldComponent,
                    ZeusCPFDirective,
                    ZeusCNPJDirective,
                    ZeusCEPDirective,
                    ZeusDocumentDirective,
                    ZeusTelefoneDirective
                ],
                imports: [
                    ZeusMaterialModule,
                    common.CommonModule,
                    forms.FormsModule,
                    forms.ReactiveFormsModule,
                ],
                exports: [ZeusMaterialModule, FieldComponent]
            })
        ], ZeusFormModule);
        return ZeusFormModule;
    }());

    exports.ZeusFormModule = ZeusFormModule;
    exports.ɵa = FieldComponent;
    exports.ɵb = ZeusCPFDirective;
    exports.ɵc = ZeusCNPJDirective;
    exports.ɵd = ZeusCEPDirective;
    exports.ɵe = ZeusDocumentValidator;
    exports.ɵf = ZeusDocumentDirective;
    exports.ɵg = ZeusTelefoneDirective;
    exports.ɵh = ZeusMaterialModule;

    Object.defineProperty(exports, '__esModule', { value: true });

}));
//# sourceMappingURL=zeus-forms.umd.js.map
