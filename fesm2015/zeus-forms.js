import { __decorate, __metadata } from 'tslib';
import { NgModule, EventEmitter, Input, Output, Component, Directive, HostListener, ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormControl, FormBuilder, NG_VALIDATORS, AbstractControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule, MatSelectModule, MatButtonModule, MatOptionModule, MatCheckboxModule, MatRadioModule, MatButtonToggleModule, MatDatepickerModule, MatFormFieldModule, MatCardModule } from '@angular/material';

const modules = [
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatOptionModule,
    MatCheckboxModule,
    MatRadioModule,
    MatButtonToggleModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatCardModule
];
let ZeusMaterialModule = class ZeusMaterialModule {
};
ZeusMaterialModule = __decorate([
    NgModule({
        declarations: [],
        imports: [
            CommonModule,
            ...modules
        ],
        exports: [
            ...modules
        ]
    })
], ZeusMaterialModule);

let FieldComponent = class FieldComponent {
    constructor(fb) {
        this.fb = fb;
        this.multiple = false;
        this.control = this.fb.control({
            item: [this.multiple ? [] : null]
        });
        this.complete = new EventEmitter();
    }
    ngOnInit() {
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], FieldComponent.prototype, "type", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], FieldComponent.prototype, "placeholder", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], FieldComponent.prototype, "options", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], FieldComponent.prototype, "label", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], FieldComponent.prototype, "multiple", void 0);
__decorate([
    Input(),
    __metadata("design:type", FormControl)
], FieldComponent.prototype, "control", void 0);
__decorate([
    Output(),
    __metadata("design:type", Object)
], FieldComponent.prototype, "complete", void 0);
FieldComponent = __decorate([
    Component({
        selector: 'zeus-field',
        template: "<ng-container [ngSwitch]=\"type\">\r\n\r\n\r\n  <ng-container *ngSwitchDefault>\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'textarea'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <textarea matInput [formControl]=\"control\" class=\"zeus-field\" rows=\"8\" [placeholder]=\"placeholder\"></textarea>\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'cpf'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" zeusCPF class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'cnpj'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" zeusCNPJ class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'cep'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" zeusCEP class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'telefone'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" zeusTelefone class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'select'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <mat-select [formControl]=\"control\" class=\"zeus-field\" [class.multiple]=\"multiple\" [multiple]=\"multiple\"\r\n        [placeholder]=\"placeholder\">\r\n        <mat-option *ngFor=\"let option of options\" [value]=\"option.value\">\r\n          {{option.label}}\r\n        </mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'checkbox'\">\r\n    <div class=\"zeus-form-field\">\r\n      <mat-checkbox [formControl]=\"control\" class=\"zeus-checkbox\">\r\n        {{label}}\r\n      </mat-checkbox>\r\n    </div>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'radio'\">\r\n    <mat-label class=\"zeus-label\">{{label}}</mat-label>\r\n    <div class=\"zeus-form-field\">\r\n      <mat-radio-group [formControl]=\"control\" class=\"zeus-radio-group\">\r\n        <mat-radio-button class=\"zeus-radio-input\" *ngFor=\"let option of options\" [value]=\"option.value\">\r\n          {{option.label}}</mat-radio-button>\r\n      </mat-radio-group>\r\n    </div>\r\n\r\n  </ng-container>\r\n\r\n  <ng-container *ngIf=\"control?.errors && control?.touched\">\r\n\r\n    <ng-content select=\".zeus-error.zeus-error-required\" *ngIf=\"control.errors.required\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-invalid\" *ngIf=\"control.errors.pattern\"></ng-content>\r\n\r\n    <ng-content select=\".zeus-error.zeus-error-cpf-digit\" *ngIf=\"control.errors.digit\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-cpf-equal\" *ngIf=\"control.errors.equalDigits\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-cpf-length\" *ngIf=\"control.errors.length\"></ng-content>\r\n\r\n    <ng-content select=\".zeus-error.zeus-error-cnpj-digit\" *ngIf=\"control.errors.digit\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-cnpj-equal\" *ngIf=\"control.errors.equalDigits\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-cnpj-length\" *ngIf=\"control.errors.length\"></ng-content>\r\n\r\n  </ng-container>\r\n\r\n  <ng-content select=\".zeus-hint\"></ng-content>\r\n\r\n\r\n</ng-container>",
        styles: [":host{display:block}:host .zeus-form-field{width:100%}"]
    }),
    __metadata("design:paramtypes", [FormBuilder])
], FieldComponent);

var ZeusDocumentDirective_1;
class ZeusDocumentValidator {
    /**
     * Calcula o dígito verificador do CPF ou CNPJ.
     */
    static buildDigit(arr) {
        const isCpf = arr.length < ZeusDocumentValidator.cpfLength;
        const digit = arr
            .map((val, idx) => val * ((!isCpf ? idx % 8 : idx) + 2))
            .reduce((total, current) => total + current) % ZeusDocumentValidator.cpfLength;
        if (digit < 2 && isCpf) {
            return 0;
        }
        return ZeusDocumentValidator.cpfLength - digit;
    }
    /**
     * Valida um CPF ou CNPJ de acordo com seu dígito verificador.
     */
    static validate(c) {
        const document = c.value.replace(/\D/g, '');
        // Verifica o tamanho da string.
        if ([ZeusDocumentValidator.cpfLength, ZeusDocumentValidator.cnpjLength].indexOf(document.length) < 0) {
            return { length: true };
        }
        // Verifica se todos os dígitos são iguais.
        if (/^([0-9])\1*$/.test(document)) {
            return { equalDigits: true };
        }
        // A seguir é realizado o cálculo verificador.
        const documentArr = document.split('').reverse().slice(2);
        documentArr.unshift(ZeusDocumentValidator.buildDigit(documentArr));
        documentArr.unshift(ZeusDocumentValidator.buildDigit(documentArr));
        if (document !== documentArr.reverse().join('')) {
            // Dígito verificador não é válido, resultando em falha.
            return { digit: true };
        }
        return null;
    }
    /**
     * Implementa a interface de um validator.
     */
    validate(c) {
        return ZeusDocumentValidator.validate(c);
    }
}
ZeusDocumentValidator.cpfLength = 11;
ZeusDocumentValidator.cnpjLength = 14;
let ZeusDocumentDirective = ZeusDocumentDirective_1 = class ZeusDocumentDirective extends ZeusDocumentValidator {
};
ZeusDocumentDirective = ZeusDocumentDirective_1 = __decorate([
    Directive({
        selector: '[zeusDocument]',
        providers: [{
                provide: NG_VALIDATORS,
                useExisting: ZeusDocumentDirective_1,
                multi: true
            }]
    })
], ZeusDocumentDirective);

let ZeusCPFDirective = class ZeusCPFDirective {
    constructor(input) {
        this.input = input;
    }
    ngOnInit() {
        this.formControl.setValidators(ZeusDocumentValidator.validate);
    }
    onInput(event) {
        let value = event.target.value.replace(/[^0-9]+/g, '').slice(0, 11);
        value = value.replace(/([0-9]{3})([0-9])/, '$1.$2');
        value = value.replace(/\.([0-9]{3})([0-9])/, '.$1.$2');
        value = value.replace(/([0-9])([0-9]{1,2})$/, '$1-$2');
        this.formControl.setValue(value);
    }
};
__decorate([
    Input('formControl'),
    __metadata("design:type", AbstractControl)
], ZeusCPFDirective.prototype, "formControl", void 0);
__decorate([
    HostListener('input', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], ZeusCPFDirective.prototype, "onInput", null);
ZeusCPFDirective = __decorate([
    Directive({
        selector: '[zeusCPF]',
    }),
    __metadata("design:paramtypes", [ElementRef])
], ZeusCPFDirective);

let ZeusCNPJDirective = class ZeusCNPJDirective {
    constructor(input) {
        this.input = input;
    }
    ngOnInit() {
        this.formControl.setValidators(ZeusDocumentValidator.validate);
    }
    onInput(event) {
        let value = event.target.value.replace(/[^0-9]+/g, '').slice(0, 14);
        value = value.replace(/^(\d{2})(\d)/, '$1.$2'); //Coloca ponto entre o segundo e o terceiro dígitos
        value = value.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3'); //Coloca ponto entre o quinto e o sexto dígitos
        value = value.replace(/\.(\d{3})(\d)/, '.$1/$2'); //Coloca uma barra entre o oitavo e o nono dígitos
        value = value.replace(/(\d{4})(\d)/, '$1-$2'); //Coloca um hífen depois do bloco de quatro dígitos
        this.formControl.setValue(value);
    }
};
__decorate([
    Input('formControl'),
    __metadata("design:type", AbstractControl)
], ZeusCNPJDirective.prototype, "formControl", void 0);
__decorate([
    HostListener('input', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], ZeusCNPJDirective.prototype, "onInput", null);
ZeusCNPJDirective = __decorate([
    Directive({
        selector: '[zeusCNPJ]',
    }),
    __metadata("design:paramtypes", [ElementRef])
], ZeusCNPJDirective);

let ZeusCEPDirective = class ZeusCEPDirective {
    constructor(input, host) {
        this.input = input;
        this.host = host;
    }
    ngOnInit() {
        console.log(this.host);
    }
    onInput(event) {
        let value = event.target.value.replace(/[^0-9]+/g, '').slice(0, 8);
        this.host.complete.emit({
            cep: value
        });
        value = value.replace(/^(\d{2})(\d)/, '$1.$2'); //Coloca ponto entre o segundo e o terceiro dígitos
        value = value.replace(/(\d)(\d{3})$/, '$1-$2'); //Coloca ponto entre o quinto e o sexto dígitos
        this.formControl.setValue(value);
    }
};
__decorate([
    Input('formControl'),
    __metadata("design:type", AbstractControl)
], ZeusCEPDirective.prototype, "formControl", void 0);
__decorate([
    HostListener('input', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], ZeusCEPDirective.prototype, "onInput", null);
ZeusCEPDirective = __decorate([
    Directive({
        selector: '[zeusCEP]',
    }),
    __metadata("design:paramtypes", [ElementRef, FieldComponent])
], ZeusCEPDirective);

class ZeusPhoneValidator {
    /**
     * Valida um CPF ou CNPJ de acordo com seu dígito verificador.
     */
    static validate(c) {
        const phone = c.value;
        // Verifica o tamanho da string.
        const lengthArray = [
            ZeusPhoneValidator.phoneLength,
            ZeusPhoneValidator.celphoneLength,
            ZeusPhoneValidator.phoneDDDLength,
            ZeusPhoneValidator.celphoneDDDLength
        ];
        if (lengthArray.indexOf(phone.length) < 0) {
            return { length: true };
        }
        const regexArray = [
            /^\d{4}-\d{4}$/,
            /^\d\s\d{4}-\d{4}$/,
            /^\(\d{2}\)\s\d{4}-\d{4}$/,
            /^\(\d{2}\)\s\d\s\d{4}-\d{4}$/
        ];
        const valid = regexArray.some(regex => regex.test(phone));
        if (!valid) {
            // Dígito verificador não é válido, resultando em falha.
            return { digit: true };
        }
        return null;
    }
    /**
     * Implementa a interface de um validator.
     */
    validate(c) {
        return ZeusPhoneValidator.validate(c);
    }
}
ZeusPhoneValidator.phoneLength = 9;
ZeusPhoneValidator.celphoneLength = 11;
ZeusPhoneValidator.phoneDDDLength = 14;
ZeusPhoneValidator.celphoneDDDLength = 16;

let ZeusTelefoneDirective = class ZeusTelefoneDirective {
    constructor(input, host) {
        this.input = input;
        this.host = host;
    }
    ngOnInit() {
        this.formControl.setValidators(ZeusPhoneValidator.validate);
    }
    onInput(event) {
        let value = event.target.value.replace(/[^0-9]+/g, '').slice(0, 11);
        this.host.complete.emit({
            telefone: value
        });
        if (value.length <= 8) {
            value = value.replace(/^(\d{4})(\d)/, '$1-$2');
        }
        else if (value.length <= 9) {
            value = value.replace(/^(\d)(\d)/, '$1 $2');
            value = value.replace(/(\d)(\d{4})$/, '$1-$2');
        }
        else if (value.length <= 10) {
            value = value.replace(/^(\d{2})(\d)/, '($1) $2');
            value = value.replace(/(\d)(\d{4})$/, '$1-$2');
        }
        else {
            value = value.replace(/^(\d{2})(\d)/, '($1) $2');
            value = value.replace(/(\d)(\d{8})$/, '$1 $2');
            value = value.replace(/(\d)(\d{4})$/, '$1-$2');
        }
        this.formControl.setValue(value);
    }
};
__decorate([
    Input('formControl'),
    __metadata("design:type", AbstractControl)
], ZeusTelefoneDirective.prototype, "formControl", void 0);
__decorate([
    HostListener('input', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], ZeusTelefoneDirective.prototype, "onInput", null);
ZeusTelefoneDirective = __decorate([
    Directive({
        selector: '[zeusTelefone]',
    }),
    __metadata("design:paramtypes", [ElementRef, FieldComponent])
], ZeusTelefoneDirective);

let ZeusFormModule = class ZeusFormModule {
};
ZeusFormModule = __decorate([
    NgModule({
        declarations: [
            FieldComponent,
            ZeusCPFDirective,
            ZeusCNPJDirective,
            ZeusCEPDirective,
            ZeusDocumentDirective,
            ZeusTelefoneDirective
        ],
        imports: [
            ZeusMaterialModule,
            CommonModule,
            FormsModule,
            ReactiveFormsModule,
        ],
        exports: [ZeusMaterialModule, FieldComponent]
    })
], ZeusFormModule);

/**
 * Generated bundle index. Do not edit.
 */

export { ZeusFormModule, FieldComponent as ɵa, ZeusCPFDirective as ɵb, ZeusCNPJDirective as ɵc, ZeusCEPDirective as ɵd, ZeusDocumentValidator as ɵe, ZeusDocumentDirective as ɵf, ZeusTelefoneDirective as ɵg, ZeusMaterialModule as ɵh };
//# sourceMappingURL=zeus-forms.js.map
