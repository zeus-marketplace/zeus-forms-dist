import * as tslib_1 from "tslib";
// Angular Basic
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// Angular Forms
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
// Angular Material
import { ZeusMaterialModule } from '../material/material.module';
// Components
import { FieldComponent } from './field/field.component';
import { ZeusCPFDirective } from './directives/cpf.directive';
import { ZeusDocumentDirective } from './validators/document.validator';
import { ZeusCNPJDirective } from './directives/cnpj.directive';
import { ZeusCEPDirective } from './directives/cep.directive';
import { ZeusTelefoneDirective } from './directives/telefone.directive';
let ZeusFormModule = class ZeusFormModule {
};
ZeusFormModule = tslib_1.__decorate([
    NgModule({
        declarations: [
            FieldComponent,
            ZeusCPFDirective,
            ZeusCNPJDirective,
            ZeusCEPDirective,
            ZeusDocumentDirective,
            ZeusTelefoneDirective
        ],
        imports: [
            ZeusMaterialModule,
            CommonModule,
            FormsModule,
            ReactiveFormsModule,
        ],
        exports: [ZeusMaterialModule, FieldComponent]
    })
], ZeusFormModule);
export { ZeusFormModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AemV1cy9mb3Jtcy8iLCJzb3VyY2VzIjpbImZvcm0vZm9ybS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLGdCQUFnQjtBQUNoQixPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxnQkFBZ0I7QUFDaEIsT0FBTyxFQUFFLG1CQUFtQixFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRWxFLG1CQUFtQjtBQUNuQixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUVqRSxhQUFhO0FBQ2IsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ3pELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQzlELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQ2hFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQzlELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBbUJ4RSxJQUFhLGNBQWMsR0FBM0IsTUFBYSxjQUFjO0NBQUksQ0FBQTtBQUFsQixjQUFjO0lBakIxQixRQUFRLENBQUM7UUFDUixZQUFZLEVBQUU7WUFDWixjQUFjO1lBQ2QsZ0JBQWdCO1lBQ2hCLGlCQUFpQjtZQUNqQixnQkFBZ0I7WUFDaEIscUJBQXFCO1lBQ3JCLHFCQUFxQjtTQUN0QjtRQUNELE9BQU8sRUFBRTtZQUNQLGtCQUFrQjtZQUNsQixZQUFZO1lBQ1osV0FBVztZQUNYLG1CQUFtQjtTQUNwQjtRQUNELE9BQU8sRUFBRSxDQUFDLGtCQUFrQixFQUFFLGNBQWMsQ0FBQztLQUM5QyxDQUFDO0dBQ1csY0FBYyxDQUFJO1NBQWxCLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBBbmd1bGFyIEJhc2ljXHJcbmltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcblxyXG4vLyBBbmd1bGFyIEZvcm1zXHJcbmltcG9ydCB7IFJlYWN0aXZlRm9ybXNNb2R1bGUsIEZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5cclxuLy8gQW5ndWxhciBNYXRlcmlhbFxyXG5pbXBvcnQgeyBaZXVzTWF0ZXJpYWxNb2R1bGUgfSBmcm9tICcuLi9tYXRlcmlhbC9tYXRlcmlhbC5tb2R1bGUnO1xyXG5cclxuLy8gQ29tcG9uZW50c1xyXG5pbXBvcnQgeyBGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vZmllbGQvZmllbGQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgWmV1c0NQRkRpcmVjdGl2ZSB9IGZyb20gJy4vZGlyZWN0aXZlcy9jcGYuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgWmV1c0RvY3VtZW50RGlyZWN0aXZlIH0gZnJvbSAnLi92YWxpZGF0b3JzL2RvY3VtZW50LnZhbGlkYXRvcic7XHJcbmltcG9ydCB7IFpldXNDTlBKRGlyZWN0aXZlIH0gZnJvbSAnLi9kaXJlY3RpdmVzL2NucGouZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgWmV1c0NFUERpcmVjdGl2ZSB9IGZyb20gJy4vZGlyZWN0aXZlcy9jZXAuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgWmV1c1RlbGVmb25lRGlyZWN0aXZlIH0gZnJvbSAnLi9kaXJlY3RpdmVzL3RlbGVmb25lLmRpcmVjdGl2ZSc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW1xyXG4gICAgRmllbGRDb21wb25lbnQsXHJcbiAgICBaZXVzQ1BGRGlyZWN0aXZlLFxyXG4gICAgWmV1c0NOUEpEaXJlY3RpdmUsXHJcbiAgICBaZXVzQ0VQRGlyZWN0aXZlLFxyXG4gICAgWmV1c0RvY3VtZW50RGlyZWN0aXZlLFxyXG4gICAgWmV1c1RlbGVmb25lRGlyZWN0aXZlXHJcbiAgXSxcclxuICBpbXBvcnRzOiBbXHJcbiAgICBaZXVzTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICBDb21tb25Nb2R1bGUsXHJcbiAgICBGb3Jtc01vZHVsZSxcclxuICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXHJcbiAgXSxcclxuICBleHBvcnRzOiBbWmV1c01hdGVyaWFsTW9kdWxlLCBGaWVsZENvbXBvbmVudF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFpldXNGb3JtTW9kdWxlIHsgfVxyXG4iXX0=