import * as tslib_1 from "tslib";
var ZeusDocumentDirective_1;
import { Directive } from '@angular/core';
import { NG_VALIDATORS } from '@angular/forms';
class ZeusDocumentValidator {
    /**
     * Calcula o dígito verificador do CPF ou CNPJ.
     */
    static buildDigit(arr) {
        const isCpf = arr.length < ZeusDocumentValidator.cpfLength;
        const digit = arr
            .map((val, idx) => val * ((!isCpf ? idx % 8 : idx) + 2))
            .reduce((total, current) => total + current) % ZeusDocumentValidator.cpfLength;
        if (digit < 2 && isCpf) {
            return 0;
        }
        return ZeusDocumentValidator.cpfLength - digit;
    }
    /**
     * Valida um CPF ou CNPJ de acordo com seu dígito verificador.
     */
    static validate(c) {
        const document = c.value.replace(/\D/g, '');
        // Verifica o tamanho da string.
        if ([ZeusDocumentValidator.cpfLength, ZeusDocumentValidator.cnpjLength].indexOf(document.length) < 0) {
            return { length: true };
        }
        // Verifica se todos os dígitos são iguais.
        if (/^([0-9])\1*$/.test(document)) {
            return { equalDigits: true };
        }
        // A seguir é realizado o cálculo verificador.
        const documentArr = document.split('').reverse().slice(2);
        documentArr.unshift(ZeusDocumentValidator.buildDigit(documentArr));
        documentArr.unshift(ZeusDocumentValidator.buildDigit(documentArr));
        if (document !== documentArr.reverse().join('')) {
            // Dígito verificador não é válido, resultando em falha.
            return { digit: true };
        }
        return null;
    }
    /**
     * Implementa a interface de um validator.
     */
    validate(c) {
        return ZeusDocumentValidator.validate(c);
    }
}
ZeusDocumentValidator.cpfLength = 11;
ZeusDocumentValidator.cnpjLength = 14;
let ZeusDocumentDirective = ZeusDocumentDirective_1 = class ZeusDocumentDirective extends ZeusDocumentValidator {
};
ZeusDocumentDirective = ZeusDocumentDirective_1 = tslib_1.__decorate([
    Directive({
        selector: '[zeusDocument]',
        providers: [{
                provide: NG_VALIDATORS,
                useExisting: ZeusDocumentDirective_1,
                multi: true
            }]
    })
], ZeusDocumentDirective);
export { ZeusDocumentDirective, ZeusDocumentValidator };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG9jdW1lbnQudmFsaWRhdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHpldXMvZm9ybXMvIiwic291cmNlcyI6WyJmb3JtL3ZhbGlkYXRvcnMvZG9jdW1lbnQudmFsaWRhdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxQyxPQUFPLEVBQWEsYUFBYSxFQUFxQyxNQUFNLGdCQUFnQixDQUFDO0FBRTdGLE1BQU0scUJBQXFCO0lBS3ZCOztPQUVHO0lBQ0gsTUFBTSxDQUFDLFVBQVUsQ0FBQyxHQUFhO1FBRTNCLE1BQU0sS0FBSyxHQUFHLEdBQUcsQ0FBQyxNQUFNLEdBQUcscUJBQXFCLENBQUMsU0FBUyxDQUFDO1FBQzNELE1BQU0sS0FBSyxHQUFHLEdBQUc7YUFDUixHQUFHLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzthQUN2RCxNQUFNLENBQUMsQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUUsQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLEdBQUcscUJBQXFCLENBQUMsU0FBUyxDQUFDO1FBRXZGLElBQUksS0FBSyxHQUFHLENBQUMsSUFBSSxLQUFLLEVBQUU7WUFDcEIsT0FBTyxDQUFDLENBQUM7U0FDWjtRQUVELE9BQU8scUJBQXFCLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztJQUNuRCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxNQUFNLENBQUMsUUFBUSxDQUFDLENBQWtCO1FBRTlCLE1BQU0sUUFBUSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQztRQUU1QyxnQ0FBZ0M7UUFDaEMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsRUFBRSxxQkFBcUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUNsRyxPQUFPLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDO1NBQzNCO1FBRUQsMkNBQTJDO1FBQzNDLElBQUksY0FBYyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUMvQixPQUFPLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRSxDQUFDO1NBQ2hDO1FBRUQsOENBQThDO1FBQzlDLE1BQU0sV0FBVyxHQUFhLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRXBFLFdBQVcsQ0FBQyxPQUFPLENBQUMscUJBQXFCLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDbkUsV0FBVyxDQUFDLE9BQU8sQ0FBQyxxQkFBcUIsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztRQUVuRSxJQUFJLFFBQVEsS0FBSyxXQUFXLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFO1lBQzdDLHdEQUF3RDtZQUN4RCxPQUFPLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDO1NBQzFCO1FBRUQsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVEOztPQUVHO0lBQ0gsUUFBUSxDQUFDLENBQWtCO1FBQ3ZCLE9BQU8scUJBQXFCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzdDLENBQUM7O0FBeERNLCtCQUFTLEdBQUcsRUFBRSxDQUFDO0FBQ2YsZ0NBQVUsR0FBRyxFQUFFLENBQUM7QUFrRTNCLElBQU0scUJBQXFCLDZCQUEzQixNQUFNLHFCQUFzQixTQUFRLHFCQUFxQjtDQUF5QixDQUFBO0FBQTVFLHFCQUFxQjtJQVIxQixTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsZ0JBQWdCO1FBQzFCLFNBQVMsRUFBRSxDQUFDO2dCQUNSLE9BQU8sRUFBRSxhQUFhO2dCQUN0QixXQUFXLEVBQUUsdUJBQXFCO2dCQUNsQyxLQUFLLEVBQUUsSUFBSTthQUNkLENBQUM7S0FDTCxDQUFDO0dBQ0kscUJBQXFCLENBQXVEO0FBRWxGLE9BQU8sRUFDSCxxQkFBcUIsRUFDckIscUJBQXFCLEVBQ3hCLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgVmFsaWRhdG9yLCBOR19WQUxJREFUT1JTLCBBYnN0cmFjdENvbnRyb2wsIFZhbGlkYXRpb25FcnJvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG5jbGFzcyBaZXVzRG9jdW1lbnRWYWxpZGF0b3IgaW1wbGVtZW50cyBWYWxpZGF0b3Ige1xyXG5cclxuICAgIHN0YXRpYyBjcGZMZW5ndGggPSAxMTtcclxuICAgIHN0YXRpYyBjbnBqTGVuZ3RoID0gMTQ7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDYWxjdWxhIG8gZMOtZ2l0byB2ZXJpZmljYWRvciBkbyBDUEYgb3UgQ05QSi5cclxuICAgICAqL1xyXG4gICAgc3RhdGljIGJ1aWxkRGlnaXQoYXJyOiBudW1iZXJbXSk6IG51bWJlciB7XHJcblxyXG4gICAgICAgIGNvbnN0IGlzQ3BmID0gYXJyLmxlbmd0aCA8IFpldXNEb2N1bWVudFZhbGlkYXRvci5jcGZMZW5ndGg7XHJcbiAgICAgICAgY29uc3QgZGlnaXQgPSBhcnJcclxuICAgICAgICAgICAgICAgIC5tYXAoKHZhbCwgaWR4KSA9PiB2YWwgKiAoKCFpc0NwZiA/IGlkeCAlIDggOiBpZHgpICsgMikpXHJcbiAgICAgICAgICAgICAgICAucmVkdWNlKCh0b3RhbCwgY3VycmVudCkgPT4gdG90YWwgKyBjdXJyZW50KSAlIFpldXNEb2N1bWVudFZhbGlkYXRvci5jcGZMZW5ndGg7XHJcblxyXG4gICAgICAgIGlmIChkaWdpdCA8IDIgJiYgaXNDcGYpIHtcclxuICAgICAgICAgICAgcmV0dXJuIDA7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gWmV1c0RvY3VtZW50VmFsaWRhdG9yLmNwZkxlbmd0aCAtIGRpZ2l0O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVmFsaWRhIHVtIENQRiBvdSBDTlBKIGRlIGFjb3JkbyBjb20gc2V1IGTDrWdpdG8gdmVyaWZpY2Fkb3IuXHJcbiAgICAgKi9cclxuICAgIHN0YXRpYyB2YWxpZGF0ZShjOiBBYnN0cmFjdENvbnRyb2wpOiBWYWxpZGF0aW9uRXJyb3JzIHwgbnVsbCB7XHJcblxyXG4gICAgICAgIGNvbnN0IGRvY3VtZW50ID0gYy52YWx1ZS5yZXBsYWNlKC9cXEQvZywgJycpO1xyXG5cclxuICAgICAgICAvLyBWZXJpZmljYSBvIHRhbWFuaG8gZGEgc3RyaW5nLlxyXG4gICAgICAgIGlmIChbWmV1c0RvY3VtZW50VmFsaWRhdG9yLmNwZkxlbmd0aCwgWmV1c0RvY3VtZW50VmFsaWRhdG9yLmNucGpMZW5ndGhdLmluZGV4T2YoZG9jdW1lbnQubGVuZ3RoKSA8IDApIHtcclxuICAgICAgICAgICAgcmV0dXJuIHsgbGVuZ3RoOiB0cnVlIH07XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBWZXJpZmljYSBzZSB0b2RvcyBvcyBkw61naXRvcyBzw6NvIGlndWFpcy5cclxuICAgICAgICBpZiAoL14oWzAtOV0pXFwxKiQvLnRlc3QoZG9jdW1lbnQpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB7IGVxdWFsRGlnaXRzOiB0cnVlIH07XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBBIHNlZ3VpciDDqSByZWFsaXphZG8gbyBjw6FsY3VsbyB2ZXJpZmljYWRvci5cclxuICAgICAgICBjb25zdCBkb2N1bWVudEFycjogbnVtYmVyW10gPSBkb2N1bWVudC5zcGxpdCgnJykucmV2ZXJzZSgpLnNsaWNlKDIpO1xyXG5cclxuICAgICAgICBkb2N1bWVudEFyci51bnNoaWZ0KFpldXNEb2N1bWVudFZhbGlkYXRvci5idWlsZERpZ2l0KGRvY3VtZW50QXJyKSk7XHJcbiAgICAgICAgZG9jdW1lbnRBcnIudW5zaGlmdChaZXVzRG9jdW1lbnRWYWxpZGF0b3IuYnVpbGREaWdpdChkb2N1bWVudEFycikpO1xyXG5cclxuICAgICAgICBpZiAoZG9jdW1lbnQgIT09IGRvY3VtZW50QXJyLnJldmVyc2UoKS5qb2luKCcnKSkge1xyXG4gICAgICAgICAgICAvLyBEw61naXRvIHZlcmlmaWNhZG9yIG7Do28gw6kgdsOhbGlkbywgcmVzdWx0YW5kbyBlbSBmYWxoYS5cclxuICAgICAgICAgICAgcmV0dXJuIHsgZGlnaXQ6IHRydWUgfTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogSW1wbGVtZW50YSBhIGludGVyZmFjZSBkZSB1bSB2YWxpZGF0b3IuXHJcbiAgICAgKi9cclxuICAgIHZhbGlkYXRlKGM6IEFic3RyYWN0Q29udHJvbCk6IFZhbGlkYXRpb25FcnJvcnMgfCBudWxsIHtcclxuICAgICAgICByZXR1cm4gWmV1c0RvY3VtZW50VmFsaWRhdG9yLnZhbGlkYXRlKGMpO1xyXG4gICAgfVxyXG59XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW3pldXNEb2N1bWVudF0nLFxyXG4gICAgcHJvdmlkZXJzOiBbe1xyXG4gICAgICAgIHByb3ZpZGU6IE5HX1ZBTElEQVRPUlMsXHJcbiAgICAgICAgdXNlRXhpc3Rpbmc6IFpldXNEb2N1bWVudERpcmVjdGl2ZSxcclxuICAgICAgICBtdWx0aTogdHJ1ZVxyXG4gICAgfV1cclxufSlcclxuY2xhc3MgWmV1c0RvY3VtZW50RGlyZWN0aXZlIGV4dGVuZHMgWmV1c0RvY3VtZW50VmFsaWRhdG9yIGltcGxlbWVudHMgVmFsaWRhdG9yIHsgfVxyXG5cclxuZXhwb3J0IHtcclxuICAgIFpldXNEb2N1bWVudERpcmVjdGl2ZSxcclxuICAgIFpldXNEb2N1bWVudFZhbGlkYXRvclxyXG59Il19