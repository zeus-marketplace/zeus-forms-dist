export class ZeusPhoneValidator {
    /**
     * Valida um CPF ou CNPJ de acordo com seu dígito verificador.
     */
    static validate(c) {
        const phone = c.value;
        // Verifica o tamanho da string.
        const lengthArray = [
            ZeusPhoneValidator.phoneLength,
            ZeusPhoneValidator.celphoneLength,
            ZeusPhoneValidator.phoneDDDLength,
            ZeusPhoneValidator.celphoneDDDLength
        ];
        if (lengthArray.indexOf(phone.length) < 0) {
            return { length: true };
        }
        const regexArray = [
            /^\d{4}-\d{4}$/,
            /^\d\s\d{4}-\d{4}$/,
            /^\(\d{2}\)\s\d{4}-\d{4}$/,
            /^\(\d{2}\)\s\d\s\d{4}-\d{4}$/
        ];
        const valid = regexArray.some(regex => regex.test(phone));
        if (!valid) {
            // Dígito verificador não é válido, resultando em falha.
            return { digit: true };
        }
        return null;
    }
    /**
     * Implementa a interface de um validator.
     */
    validate(c) {
        return ZeusPhoneValidator.validate(c);
    }
}
ZeusPhoneValidator.phoneLength = 9;
ZeusPhoneValidator.celphoneLength = 11;
ZeusPhoneValidator.phoneDDDLength = 14;
ZeusPhoneValidator.celphoneDDDLength = 16;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGhvbmUudmFsaWRhdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHpldXMvZm9ybXMvIiwic291cmNlcyI6WyJmb3JtL3ZhbGlkYXRvcnMvcGhvbmUudmFsaWRhdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sT0FBTyxrQkFBa0I7SUFPM0I7O09BRUc7SUFDSCxNQUFNLENBQUMsUUFBUSxDQUFDLENBQWtCO1FBRTlCLE1BQU0sS0FBSyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFFdEIsZ0NBQWdDO1FBQ2hDLE1BQU0sV0FBVyxHQUFHO1lBQ2hCLGtCQUFrQixDQUFDLFdBQVc7WUFDOUIsa0JBQWtCLENBQUMsY0FBYztZQUNqQyxrQkFBa0IsQ0FBQyxjQUFjO1lBQ2pDLGtCQUFrQixDQUFDLGlCQUFpQjtTQUN2QyxDQUFDO1FBQ0YsSUFBSSxXQUFXLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDdkMsT0FBTyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQztTQUMzQjtRQUVELE1BQU0sVUFBVSxHQUFhO1lBQ3pCLGVBQWU7WUFDZixtQkFBbUI7WUFDbkIsMEJBQTBCO1lBQzFCLDhCQUE4QjtTQUNqQyxDQUFDO1FBQ0YsTUFBTSxLQUFLLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUUxRCxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1Isd0RBQXdEO1lBQ3hELE9BQU8sRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUM7U0FDMUI7UUFFRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQ7O09BRUc7SUFDSCxRQUFRLENBQUMsQ0FBa0I7UUFDdkIsT0FBTyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7QUE1Q00sOEJBQVcsR0FBRyxDQUFDLENBQUM7QUFDaEIsaUNBQWMsR0FBRyxFQUFFLENBQUM7QUFDcEIsaUNBQWMsR0FBRyxFQUFFLENBQUM7QUFDcEIsb0NBQWlCLEdBQUcsRUFBRSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgVmFsaWRhdG9yLCBBYnN0cmFjdENvbnRyb2wsIFZhbGlkYXRpb25FcnJvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG5leHBvcnQgY2xhc3MgWmV1c1Bob25lVmFsaWRhdG9yIGltcGxlbWVudHMgVmFsaWRhdG9yIHtcclxuXHJcbiAgICBzdGF0aWMgcGhvbmVMZW5ndGggPSA5O1xyXG4gICAgc3RhdGljIGNlbHBob25lTGVuZ3RoID0gMTE7XHJcbiAgICBzdGF0aWMgcGhvbmVERERMZW5ndGggPSAxNDtcclxuICAgIHN0YXRpYyBjZWxwaG9uZURERExlbmd0aCA9IDE2O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogVmFsaWRhIHVtIENQRiBvdSBDTlBKIGRlIGFjb3JkbyBjb20gc2V1IGTDrWdpdG8gdmVyaWZpY2Fkb3IuXHJcbiAgICAgKi9cclxuICAgIHN0YXRpYyB2YWxpZGF0ZShjOiBBYnN0cmFjdENvbnRyb2wpOiBWYWxpZGF0aW9uRXJyb3JzIHwgbnVsbCB7XHJcblxyXG4gICAgICAgIGNvbnN0IHBob25lID0gYy52YWx1ZTtcclxuXHJcbiAgICAgICAgLy8gVmVyaWZpY2EgbyB0YW1hbmhvIGRhIHN0cmluZy5cclxuICAgICAgICBjb25zdCBsZW5ndGhBcnJheSA9IFtcclxuICAgICAgICAgICAgWmV1c1Bob25lVmFsaWRhdG9yLnBob25lTGVuZ3RoLCBcclxuICAgICAgICAgICAgWmV1c1Bob25lVmFsaWRhdG9yLmNlbHBob25lTGVuZ3RoLCBcclxuICAgICAgICAgICAgWmV1c1Bob25lVmFsaWRhdG9yLnBob25lRERETGVuZ3RoLCBcclxuICAgICAgICAgICAgWmV1c1Bob25lVmFsaWRhdG9yLmNlbHBob25lRERETGVuZ3RoXHJcbiAgICAgICAgXTtcclxuICAgICAgICBpZiAobGVuZ3RoQXJyYXkuaW5kZXhPZihwaG9uZS5sZW5ndGgpIDwgMCkge1xyXG4gICAgICAgICAgICByZXR1cm4geyBsZW5ndGg6IHRydWUgfTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHJlZ2V4QXJyYXk6IFJlZ0V4cFtdID0gW1xyXG4gICAgICAgICAgICAvXlxcZHs0fS1cXGR7NH0kLyxcclxuICAgICAgICAgICAgL15cXGRcXHNcXGR7NH0tXFxkezR9JC8sXHJcbiAgICAgICAgICAgIC9eXFwoXFxkezJ9XFwpXFxzXFxkezR9LVxcZHs0fSQvLFxyXG4gICAgICAgICAgICAvXlxcKFxcZHsyfVxcKVxcc1xcZFxcc1xcZHs0fS1cXGR7NH0kL1xyXG4gICAgICAgIF07XHJcbiAgICAgICAgY29uc3QgdmFsaWQgPSByZWdleEFycmF5LnNvbWUocmVnZXggPT4gcmVnZXgudGVzdChwaG9uZSkpO1xyXG5cclxuICAgICAgICBpZiAoIXZhbGlkKSB7XHJcbiAgICAgICAgICAgIC8vIETDrWdpdG8gdmVyaWZpY2Fkb3IgbsOjbyDDqSB2w6FsaWRvLCByZXN1bHRhbmRvIGVtIGZhbGhhLlxyXG4gICAgICAgICAgICByZXR1cm4geyBkaWdpdDogdHJ1ZSB9O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBJbXBsZW1lbnRhIGEgaW50ZXJmYWNlIGRlIHVtIHZhbGlkYXRvci5cclxuICAgICAqL1xyXG4gICAgdmFsaWRhdGUoYzogQWJzdHJhY3RDb250cm9sKTogVmFsaWRhdGlvbkVycm9ycyB8IG51bGwge1xyXG4gICAgICAgIHJldHVybiBaZXVzUGhvbmVWYWxpZGF0b3IudmFsaWRhdGUoYyk7XHJcbiAgICB9XHJcbn0iXX0=