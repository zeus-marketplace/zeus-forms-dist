import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormBuilder } from '@angular/forms';
let FieldComponent = class FieldComponent {
    constructor(fb) {
        this.fb = fb;
        this.multiple = false;
        this.control = this.fb.control({
            item: [this.multiple ? [] : null]
        });
        this.complete = new EventEmitter();
    }
    ngOnInit() {
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], FieldComponent.prototype, "type", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], FieldComponent.prototype, "placeholder", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], FieldComponent.prototype, "options", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], FieldComponent.prototype, "label", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], FieldComponent.prototype, "multiple", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", FormControl)
], FieldComponent.prototype, "control", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", Object)
], FieldComponent.prototype, "complete", void 0);
FieldComponent = tslib_1.__decorate([
    Component({
        selector: 'zeus-field',
        template: "<ng-container [ngSwitch]=\"type\">\r\n\r\n\r\n  <ng-container *ngSwitchDefault>\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'textarea'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <textarea matInput [formControl]=\"control\" class=\"zeus-field\" rows=\"8\" [placeholder]=\"placeholder\"></textarea>\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'cpf'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" zeusCPF class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'cnpj'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" zeusCNPJ class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'cep'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" zeusCEP class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'telefone'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" zeusTelefone class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'select'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <mat-select [formControl]=\"control\" class=\"zeus-field\" [class.multiple]=\"multiple\" [multiple]=\"multiple\"\r\n        [placeholder]=\"placeholder\">\r\n        <mat-option *ngFor=\"let option of options\" [value]=\"option.value\">\r\n          {{option.label}}\r\n        </mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'checkbox'\">\r\n    <div class=\"zeus-form-field\">\r\n      <mat-checkbox [formControl]=\"control\" class=\"zeus-checkbox\">\r\n        {{label}}\r\n      </mat-checkbox>\r\n    </div>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'radio'\">\r\n    <mat-label class=\"zeus-label\">{{label}}</mat-label>\r\n    <div class=\"zeus-form-field\">\r\n      <mat-radio-group [formControl]=\"control\" class=\"zeus-radio-group\">\r\n        <mat-radio-button class=\"zeus-radio-input\" *ngFor=\"let option of options\" [value]=\"option.value\">\r\n          {{option.label}}</mat-radio-button>\r\n      </mat-radio-group>\r\n    </div>\r\n\r\n  </ng-container>\r\n\r\n  <ng-container *ngIf=\"control?.errors && control?.touched\">\r\n\r\n    <ng-content select=\".zeus-error.zeus-error-required\" *ngIf=\"control.errors.required\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-invalid\" *ngIf=\"control.errors.pattern\"></ng-content>\r\n\r\n    <ng-content select=\".zeus-error.zeus-error-cpf-digit\" *ngIf=\"control.errors.digit\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-cpf-equal\" *ngIf=\"control.errors.equalDigits\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-cpf-length\" *ngIf=\"control.errors.length\"></ng-content>\r\n\r\n    <ng-content select=\".zeus-error.zeus-error-cnpj-digit\" *ngIf=\"control.errors.digit\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-cnpj-equal\" *ngIf=\"control.errors.equalDigits\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-cnpj-length\" *ngIf=\"control.errors.length\"></ng-content>\r\n\r\n  </ng-container>\r\n\r\n  <ng-content select=\".zeus-hint\"></ng-content>\r\n\r\n\r\n</ng-container>",
        styles: [":host{display:block}:host .zeus-form-field{width:100%}"]
    }),
    tslib_1.__metadata("design:paramtypes", [FormBuilder])
], FieldComponent);
export { FieldComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHpldXMvZm9ybXMvIiwic291cmNlcyI6WyJmb3JtL2ZpZWxkL2ZpZWxkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMvRSxPQUFPLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBTzFELElBQWEsY0FBYyxHQUEzQixNQUFhLGNBQWM7SUFjekIsWUFDVSxFQUFlO1FBQWYsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQVRoQixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLFlBQU8sR0FBZ0IsSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7WUFDOUMsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7U0FDbEMsQ0FBQyxDQUFDO1FBRU8sYUFBUSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7SUFLcEMsQ0FBQztJQUVMLFFBQVE7SUFDUixDQUFDO0NBRUYsQ0FBQTtBQW5CVTtJQUFSLEtBQUssRUFBRTs7NENBQU07QUFDTDtJQUFSLEtBQUssRUFBRTs7bURBQWE7QUFDWjtJQUFSLEtBQUssRUFBRTs7K0NBQVM7QUFDUjtJQUFSLEtBQUssRUFBRTs7NkNBQU87QUFDTjtJQUFSLEtBQUssRUFBRTs7Z0RBQWtCO0FBQ2pCO0lBQVIsS0FBSyxFQUFFO3NDQUFVLFdBQVc7K0NBRTFCO0FBRU87SUFBVCxNQUFNLEVBQUU7O2dEQUErQjtBQVg3QixjQUFjO0lBTDFCLFNBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxZQUFZO1FBQ3RCLDh3SEFBcUM7O0tBRXRDLENBQUM7NkNBZ0JjLFdBQVc7R0FmZCxjQUFjLENBcUIxQjtTQXJCWSxjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtQ29udHJvbCwgRm9ybUJ1aWxkZXIgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ3pldXMtZmllbGQnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9maWVsZC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZmllbGQuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRmllbGRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIFxyXG4gIEBJbnB1dCgpIHR5cGU7XHJcbiAgQElucHV0KCkgcGxhY2Vob2xkZXI7XHJcbiAgQElucHV0KCkgb3B0aW9ucztcclxuICBASW5wdXQoKSBsYWJlbDtcclxuICBASW5wdXQoKSBtdWx0aXBsZSA9IGZhbHNlOyBcclxuICBASW5wdXQoKSBjb250cm9sOiBGb3JtQ29udHJvbCA9IHRoaXMuZmIuY29udHJvbCh7XHJcbiAgICBpdGVtOiBbdGhpcy5tdWx0aXBsZSA/IFtdIDogbnVsbF1cclxuICB9KTtcclxuXHJcbiAgQE91dHB1dCgpIGNvbXBsZXRlID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gXHJcbiAgXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGZiOiBGb3JtQnVpbGRlclxyXG4gICkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbn1cclxuIl19