import * as tslib_1 from "tslib";
import { Directive, HostListener, ElementRef, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { ZeusDocumentValidator } from '../validators/document.validator';
let ZeusCNPJDirective = class ZeusCNPJDirective {
    constructor(input) {
        this.input = input;
    }
    ngOnInit() {
        this.formControl.setValidators(ZeusDocumentValidator.validate);
    }
    onInput(event) {
        let value = event.target.value.replace(/[^0-9]+/g, '').slice(0, 14);
        value = value.replace(/^(\d{2})(\d)/, '$1.$2'); //Coloca ponto entre o segundo e o terceiro dígitos
        value = value.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3'); //Coloca ponto entre o quinto e o sexto dígitos
        value = value.replace(/\.(\d{3})(\d)/, '.$1/$2'); //Coloca uma barra entre o oitavo e o nono dígitos
        value = value.replace(/(\d{4})(\d)/, '$1-$2'); //Coloca um hífen depois do bloco de quatro dígitos
        this.formControl.setValue(value);
    }
};
tslib_1.__decorate([
    Input('formControl'),
    tslib_1.__metadata("design:type", AbstractControl)
], ZeusCNPJDirective.prototype, "formControl", void 0);
tslib_1.__decorate([
    HostListener('input', ['$event']),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", void 0)
], ZeusCNPJDirective.prototype, "onInput", null);
ZeusCNPJDirective = tslib_1.__decorate([
    Directive({
        selector: '[zeusCNPJ]',
    }),
    tslib_1.__metadata("design:paramtypes", [ElementRef])
], ZeusCNPJDirective);
export { ZeusCNPJDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY25wai5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AemV1cy9mb3Jtcy8iLCJzb3VyY2VzIjpbImZvcm0vZGlyZWN0aXZlcy9jbnBqLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzRSxPQUFPLEVBQTBCLGVBQWUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3pFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBS3pFLElBQWEsaUJBQWlCLEdBQTlCLE1BQWEsaUJBQWlCO0lBRzFCLFlBQW9CLEtBQWlCO1FBQWpCLFVBQUssR0FBTCxLQUFLLENBQVk7SUFBSSxDQUFDO0lBRTFDLFFBQVE7UUFDSixJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNuRSxDQUFDO0lBR0QsT0FBTyxDQUFDLEtBQUs7UUFFVCxJQUFJLEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDcEUsS0FBSyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsbURBQW1EO1FBQ25HLEtBQUssR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLHVCQUF1QixFQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUMsK0NBQStDO1FBQzNHLEtBQUssR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLGVBQWUsRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDLGtEQUFrRDtRQUNwRyxLQUFLLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxtREFBbUQ7UUFFbEcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7SUFFckMsQ0FBQztDQUVKLENBQUE7QUFwQnlCO0lBQXJCLEtBQUssQ0FBQyxhQUFhLENBQUM7c0NBQWMsZUFBZTtzREFBQztBQVFuRDtJQURDLFlBQVksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQzs7OztnREFXakM7QUFwQlEsaUJBQWlCO0lBSDdCLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxZQUFZO0tBQ3pCLENBQUM7NkNBSTZCLFVBQVU7R0FINUIsaUJBQWlCLENBc0I3QjtTQXRCWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEhvc3RMaXN0ZW5lciwgRWxlbWVudFJlZiwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTmdDb250cm9sLCBGb3JtQ29udHJvbCwgQWJzdHJhY3RDb250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBaZXVzRG9jdW1lbnRWYWxpZGF0b3IgfSBmcm9tICcuLi92YWxpZGF0b3JzL2RvY3VtZW50LnZhbGlkYXRvcic7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW3pldXNDTlBKXScsXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBaZXVzQ05QSkRpcmVjdGl2ZSB7XHJcblxyXG4gICAgQElucHV0KCdmb3JtQ29udHJvbCcpIGZvcm1Db250cm9sOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGlucHV0OiBFbGVtZW50UmVmKSB7IH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmZvcm1Db250cm9sLnNldFZhbGlkYXRvcnMoWmV1c0RvY3VtZW50VmFsaWRhdG9yLnZhbGlkYXRlKTtcclxuICAgIH1cclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCdpbnB1dCcsIFsnJGV2ZW50J10pXHJcbiAgICBvbklucHV0KGV2ZW50KSB7XHJcblxyXG4gICAgICAgIGxldCB2YWx1ZSA9IGV2ZW50LnRhcmdldC52YWx1ZS5yZXBsYWNlKC9bXjAtOV0rL2csICcnKS5zbGljZSgwLCAxNCk7XHJcbiAgICAgICAgdmFsdWUgPSB2YWx1ZS5yZXBsYWNlKC9eKFxcZHsyfSkoXFxkKS8sICckMS4kMicpOyAvL0NvbG9jYSBwb250byBlbnRyZSBvIHNlZ3VuZG8gZSBvIHRlcmNlaXJvIGTDrWdpdG9zXHJcbiAgICAgICAgdmFsdWUgPSB2YWx1ZS5yZXBsYWNlKC9eKFxcZHsyfSlcXC4oXFxkezN9KShcXGQpLywgJyQxLiQyLiQzJyk7IC8vQ29sb2NhIHBvbnRvIGVudHJlIG8gcXVpbnRvIGUgbyBzZXh0byBkw61naXRvc1xyXG4gICAgICAgIHZhbHVlID0gdmFsdWUucmVwbGFjZSgvXFwuKFxcZHszfSkoXFxkKS8sICcuJDEvJDInKTsgLy9Db2xvY2EgdW1hIGJhcnJhIGVudHJlIG8gb2l0YXZvIGUgbyBub25vIGTDrWdpdG9zXHJcbiAgICAgICAgdmFsdWUgPSB2YWx1ZS5yZXBsYWNlKC8oXFxkezR9KShcXGQpLywgJyQxLSQyJyk7IC8vQ29sb2NhIHVtIGjDrWZlbiBkZXBvaXMgZG8gYmxvY28gZGUgcXVhdHJvIGTDrWdpdG9zXHJcblxyXG4gICAgICAgIHRoaXMuZm9ybUNvbnRyb2wuc2V0VmFsdWUodmFsdWUpO1xyXG5cclxuICAgIH1cclxuXHJcbn0iXX0=