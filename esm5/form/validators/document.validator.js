import * as tslib_1 from "tslib";
import { Directive } from '@angular/core';
import { NG_VALIDATORS } from '@angular/forms';
var ZeusDocumentValidator = /** @class */ (function () {
    function ZeusDocumentValidator() {
    }
    /**
     * Calcula o dígito verificador do CPF ou CNPJ.
     */
    ZeusDocumentValidator.buildDigit = function (arr) {
        var isCpf = arr.length < ZeusDocumentValidator.cpfLength;
        var digit = arr
            .map(function (val, idx) { return val * ((!isCpf ? idx % 8 : idx) + 2); })
            .reduce(function (total, current) { return total + current; }) % ZeusDocumentValidator.cpfLength;
        if (digit < 2 && isCpf) {
            return 0;
        }
        return ZeusDocumentValidator.cpfLength - digit;
    };
    /**
     * Valida um CPF ou CNPJ de acordo com seu dígito verificador.
     */
    ZeusDocumentValidator.validate = function (c) {
        var document = c.value.replace(/\D/g, '');
        // Verifica o tamanho da string.
        if ([ZeusDocumentValidator.cpfLength, ZeusDocumentValidator.cnpjLength].indexOf(document.length) < 0) {
            return { length: true };
        }
        // Verifica se todos os dígitos são iguais.
        if (/^([0-9])\1*$/.test(document)) {
            return { equalDigits: true };
        }
        // A seguir é realizado o cálculo verificador.
        var documentArr = document.split('').reverse().slice(2);
        documentArr.unshift(ZeusDocumentValidator.buildDigit(documentArr));
        documentArr.unshift(ZeusDocumentValidator.buildDigit(documentArr));
        if (document !== documentArr.reverse().join('')) {
            // Dígito verificador não é válido, resultando em falha.
            return { digit: true };
        }
        return null;
    };
    /**
     * Implementa a interface de um validator.
     */
    ZeusDocumentValidator.prototype.validate = function (c) {
        return ZeusDocumentValidator.validate(c);
    };
    ZeusDocumentValidator.cpfLength = 11;
    ZeusDocumentValidator.cnpjLength = 14;
    return ZeusDocumentValidator;
}());
var ZeusDocumentDirective = /** @class */ (function (_super) {
    tslib_1.__extends(ZeusDocumentDirective, _super);
    function ZeusDocumentDirective() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ZeusDocumentDirective_1 = ZeusDocumentDirective;
    var ZeusDocumentDirective_1;
    ZeusDocumentDirective = ZeusDocumentDirective_1 = tslib_1.__decorate([
        Directive({
            selector: '[zeusDocument]',
            providers: [{
                    provide: NG_VALIDATORS,
                    useExisting: ZeusDocumentDirective_1,
                    multi: true
                }]
        })
    ], ZeusDocumentDirective);
    return ZeusDocumentDirective;
}(ZeusDocumentValidator));
export { ZeusDocumentDirective, ZeusDocumentValidator };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG9jdW1lbnQudmFsaWRhdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHpldXMvZm9ybXMvIiwic291cmNlcyI6WyJmb3JtL3ZhbGlkYXRvcnMvZG9jdW1lbnQudmFsaWRhdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFDLE9BQU8sRUFBYSxhQUFhLEVBQXFDLE1BQU0sZ0JBQWdCLENBQUM7QUFFN0Y7SUFBQTtJQTJEQSxDQUFDO0lBdERHOztPQUVHO0lBQ0ksZ0NBQVUsR0FBakIsVUFBa0IsR0FBYTtRQUUzQixJQUFNLEtBQUssR0FBRyxHQUFHLENBQUMsTUFBTSxHQUFHLHFCQUFxQixDQUFDLFNBQVMsQ0FBQztRQUMzRCxJQUFNLEtBQUssR0FBRyxHQUFHO2FBQ1IsR0FBRyxDQUFDLFVBQUMsR0FBRyxFQUFFLEdBQUcsSUFBSyxPQUFBLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFwQyxDQUFvQyxDQUFDO2FBQ3ZELE1BQU0sQ0FBQyxVQUFDLEtBQUssRUFBRSxPQUFPLElBQUssT0FBQSxLQUFLLEdBQUcsT0FBTyxFQUFmLENBQWUsQ0FBQyxHQUFHLHFCQUFxQixDQUFDLFNBQVMsQ0FBQztRQUV2RixJQUFJLEtBQUssR0FBRyxDQUFDLElBQUksS0FBSyxFQUFFO1lBQ3BCLE9BQU8sQ0FBQyxDQUFDO1NBQ1o7UUFFRCxPQUFPLHFCQUFxQixDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7SUFDbkQsQ0FBQztJQUVEOztPQUVHO0lBQ0ksOEJBQVEsR0FBZixVQUFnQixDQUFrQjtRQUU5QixJQUFNLFFBQVEsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFFNUMsZ0NBQWdDO1FBQ2hDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLEVBQUUscUJBQXFCLENBQUMsVUFBVSxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDbEcsT0FBTyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQztTQUMzQjtRQUVELDJDQUEyQztRQUMzQyxJQUFJLGNBQWMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDL0IsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUUsQ0FBQztTQUNoQztRQUVELDhDQUE4QztRQUM5QyxJQUFNLFdBQVcsR0FBYSxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUVwRSxXQUFXLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQ25FLFdBQVcsQ0FBQyxPQUFPLENBQUMscUJBQXFCLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFFbkUsSUFBSSxRQUFRLEtBQUssV0FBVyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRTtZQUM3Qyx3REFBd0Q7WUFDeEQsT0FBTyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsQ0FBQztTQUMxQjtRQUVELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7T0FFRztJQUNILHdDQUFRLEdBQVIsVUFBUyxDQUFrQjtRQUN2QixPQUFPLHFCQUFxQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBeERNLCtCQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ2YsZ0NBQVUsR0FBRyxFQUFFLENBQUM7SUF3RDNCLDRCQUFDO0NBQUEsQUEzREQsSUEyREM7QUFVRDtJQUFvQyxpREFBcUI7SUFBekQ7O0lBQWlGLENBQUM7OEJBQTVFLHFCQUFxQjs7SUFBckIscUJBQXFCO1FBUjFCLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxnQkFBZ0I7WUFDMUIsU0FBUyxFQUFFLENBQUM7b0JBQ1IsT0FBTyxFQUFFLGFBQWE7b0JBQ3RCLFdBQVcsRUFBRSx1QkFBcUI7b0JBQ2xDLEtBQUssRUFBRSxJQUFJO2lCQUNkLENBQUM7U0FDTCxDQUFDO09BQ0kscUJBQXFCLENBQXVEO0lBQUQsNEJBQUM7Q0FBQSxBQUFsRixDQUFvQyxxQkFBcUIsR0FBeUI7QUFFbEYsT0FBTyxFQUNILHFCQUFxQixFQUNyQixxQkFBcUIsRUFDeEIsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBWYWxpZGF0b3IsIE5HX1ZBTElEQVRPUlMsIEFic3RyYWN0Q29udHJvbCwgVmFsaWRhdGlvbkVycm9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbmNsYXNzIFpldXNEb2N1bWVudFZhbGlkYXRvciBpbXBsZW1lbnRzIFZhbGlkYXRvciB7XHJcblxyXG4gICAgc3RhdGljIGNwZkxlbmd0aCA9IDExO1xyXG4gICAgc3RhdGljIGNucGpMZW5ndGggPSAxNDtcclxuXHJcbiAgICAvKipcclxuICAgICAqIENhbGN1bGEgbyBkw61naXRvIHZlcmlmaWNhZG9yIGRvIENQRiBvdSBDTlBKLlxyXG4gICAgICovXHJcbiAgICBzdGF0aWMgYnVpbGREaWdpdChhcnI6IG51bWJlcltdKTogbnVtYmVyIHtcclxuXHJcbiAgICAgICAgY29uc3QgaXNDcGYgPSBhcnIubGVuZ3RoIDwgWmV1c0RvY3VtZW50VmFsaWRhdG9yLmNwZkxlbmd0aDtcclxuICAgICAgICBjb25zdCBkaWdpdCA9IGFyclxyXG4gICAgICAgICAgICAgICAgLm1hcCgodmFsLCBpZHgpID0+IHZhbCAqICgoIWlzQ3BmID8gaWR4ICUgOCA6IGlkeCkgKyAyKSlcclxuICAgICAgICAgICAgICAgIC5yZWR1Y2UoKHRvdGFsLCBjdXJyZW50KSA9PiB0b3RhbCArIGN1cnJlbnQpICUgWmV1c0RvY3VtZW50VmFsaWRhdG9yLmNwZkxlbmd0aDtcclxuXHJcbiAgICAgICAgaWYgKGRpZ2l0IDwgMiAmJiBpc0NwZikge1xyXG4gICAgICAgICAgICByZXR1cm4gMDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBaZXVzRG9jdW1lbnRWYWxpZGF0b3IuY3BmTGVuZ3RoIC0gZGlnaXQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBWYWxpZGEgdW0gQ1BGIG91IENOUEogZGUgYWNvcmRvIGNvbSBzZXUgZMOtZ2l0byB2ZXJpZmljYWRvci5cclxuICAgICAqL1xyXG4gICAgc3RhdGljIHZhbGlkYXRlKGM6IEFic3RyYWN0Q29udHJvbCk6IFZhbGlkYXRpb25FcnJvcnMgfCBudWxsIHtcclxuXHJcbiAgICAgICAgY29uc3QgZG9jdW1lbnQgPSBjLnZhbHVlLnJlcGxhY2UoL1xcRC9nLCAnJyk7XHJcblxyXG4gICAgICAgIC8vIFZlcmlmaWNhIG8gdGFtYW5obyBkYSBzdHJpbmcuXHJcbiAgICAgICAgaWYgKFtaZXVzRG9jdW1lbnRWYWxpZGF0b3IuY3BmTGVuZ3RoLCBaZXVzRG9jdW1lbnRWYWxpZGF0b3IuY25wakxlbmd0aF0uaW5kZXhPZihkb2N1bWVudC5sZW5ndGgpIDwgMCkge1xyXG4gICAgICAgICAgICByZXR1cm4geyBsZW5ndGg6IHRydWUgfTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFZlcmlmaWNhIHNlIHRvZG9zIG9zIGTDrWdpdG9zIHPDo28gaWd1YWlzLlxyXG4gICAgICAgIGlmICgvXihbMC05XSlcXDEqJC8udGVzdChkb2N1bWVudCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHsgZXF1YWxEaWdpdHM6IHRydWUgfTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIEEgc2VndWlyIMOpIHJlYWxpemFkbyBvIGPDoWxjdWxvIHZlcmlmaWNhZG9yLlxyXG4gICAgICAgIGNvbnN0IGRvY3VtZW50QXJyOiBudW1iZXJbXSA9IGRvY3VtZW50LnNwbGl0KCcnKS5yZXZlcnNlKCkuc2xpY2UoMik7XHJcblxyXG4gICAgICAgIGRvY3VtZW50QXJyLnVuc2hpZnQoWmV1c0RvY3VtZW50VmFsaWRhdG9yLmJ1aWxkRGlnaXQoZG9jdW1lbnRBcnIpKTtcclxuICAgICAgICBkb2N1bWVudEFyci51bnNoaWZ0KFpldXNEb2N1bWVudFZhbGlkYXRvci5idWlsZERpZ2l0KGRvY3VtZW50QXJyKSk7XHJcblxyXG4gICAgICAgIGlmIChkb2N1bWVudCAhPT0gZG9jdW1lbnRBcnIucmV2ZXJzZSgpLmpvaW4oJycpKSB7XHJcbiAgICAgICAgICAgIC8vIETDrWdpdG8gdmVyaWZpY2Fkb3IgbsOjbyDDqSB2w6FsaWRvLCByZXN1bHRhbmRvIGVtIGZhbGhhLlxyXG4gICAgICAgICAgICByZXR1cm4geyBkaWdpdDogdHJ1ZSB9O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBJbXBsZW1lbnRhIGEgaW50ZXJmYWNlIGRlIHVtIHZhbGlkYXRvci5cclxuICAgICAqL1xyXG4gICAgdmFsaWRhdGUoYzogQWJzdHJhY3RDb250cm9sKTogVmFsaWRhdGlvbkVycm9ycyB8IG51bGwge1xyXG4gICAgICAgIHJldHVybiBaZXVzRG9jdW1lbnRWYWxpZGF0b3IudmFsaWRhdGUoYyk7XHJcbiAgICB9XHJcbn1cclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICdbemV1c0RvY3VtZW50XScsXHJcbiAgICBwcm92aWRlcnM6IFt7XHJcbiAgICAgICAgcHJvdmlkZTogTkdfVkFMSURBVE9SUyxcclxuICAgICAgICB1c2VFeGlzdGluZzogWmV1c0RvY3VtZW50RGlyZWN0aXZlLFxyXG4gICAgICAgIG11bHRpOiB0cnVlXHJcbiAgICB9XVxyXG59KVxyXG5jbGFzcyBaZXVzRG9jdW1lbnREaXJlY3RpdmUgZXh0ZW5kcyBaZXVzRG9jdW1lbnRWYWxpZGF0b3IgaW1wbGVtZW50cyBWYWxpZGF0b3IgeyB9XHJcblxyXG5leHBvcnQge1xyXG4gICAgWmV1c0RvY3VtZW50RGlyZWN0aXZlLFxyXG4gICAgWmV1c0RvY3VtZW50VmFsaWRhdG9yXHJcbn0iXX0=