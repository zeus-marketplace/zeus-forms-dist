var ZeusPhoneValidator = /** @class */ (function () {
    function ZeusPhoneValidator() {
    }
    /**
     * Valida um CPF ou CNPJ de acordo com seu dígito verificador.
     */
    ZeusPhoneValidator.validate = function (c) {
        var phone = c.value;
        // Verifica o tamanho da string.
        var lengthArray = [
            ZeusPhoneValidator.phoneLength,
            ZeusPhoneValidator.celphoneLength,
            ZeusPhoneValidator.phoneDDDLength,
            ZeusPhoneValidator.celphoneDDDLength
        ];
        if (lengthArray.indexOf(phone.length) < 0) {
            return { length: true };
        }
        var regexArray = [
            /^\d{4}-\d{4}$/,
            /^\d\s\d{4}-\d{4}$/,
            /^\(\d{2}\)\s\d{4}-\d{4}$/,
            /^\(\d{2}\)\s\d\s\d{4}-\d{4}$/
        ];
        var valid = regexArray.some(function (regex) { return regex.test(phone); });
        if (!valid) {
            // Dígito verificador não é válido, resultando em falha.
            return { digit: true };
        }
        return null;
    };
    /**
     * Implementa a interface de um validator.
     */
    ZeusPhoneValidator.prototype.validate = function (c) {
        return ZeusPhoneValidator.validate(c);
    };
    ZeusPhoneValidator.phoneLength = 9;
    ZeusPhoneValidator.celphoneLength = 11;
    ZeusPhoneValidator.phoneDDDLength = 14;
    ZeusPhoneValidator.celphoneDDDLength = 16;
    return ZeusPhoneValidator;
}());
export { ZeusPhoneValidator };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGhvbmUudmFsaWRhdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHpldXMvZm9ybXMvIiwic291cmNlcyI6WyJmb3JtL3ZhbGlkYXRvcnMvcGhvbmUudmFsaWRhdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0lBQUE7SUErQ0EsQ0FBQztJQXhDRzs7T0FFRztJQUNJLDJCQUFRLEdBQWYsVUFBZ0IsQ0FBa0I7UUFFOUIsSUFBTSxLQUFLLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUV0QixnQ0FBZ0M7UUFDaEMsSUFBTSxXQUFXLEdBQUc7WUFDaEIsa0JBQWtCLENBQUMsV0FBVztZQUM5QixrQkFBa0IsQ0FBQyxjQUFjO1lBQ2pDLGtCQUFrQixDQUFDLGNBQWM7WUFDakMsa0JBQWtCLENBQUMsaUJBQWlCO1NBQ3ZDLENBQUM7UUFDRixJQUFJLFdBQVcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUN2QyxPQUFPLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDO1NBQzNCO1FBRUQsSUFBTSxVQUFVLEdBQWE7WUFDekIsZUFBZTtZQUNmLG1CQUFtQjtZQUNuQiwwQkFBMEI7WUFDMUIsOEJBQThCO1NBQ2pDLENBQUM7UUFDRixJQUFNLEtBQUssR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBakIsQ0FBaUIsQ0FBQyxDQUFDO1FBRTFELElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDUix3REFBd0Q7WUFDeEQsT0FBTyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsQ0FBQztTQUMxQjtRQUVELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7T0FFRztJQUNILHFDQUFRLEdBQVIsVUFBUyxDQUFrQjtRQUN2QixPQUFPLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBNUNNLDhCQUFXLEdBQUcsQ0FBQyxDQUFDO0lBQ2hCLGlDQUFjLEdBQUcsRUFBRSxDQUFDO0lBQ3BCLGlDQUFjLEdBQUcsRUFBRSxDQUFDO0lBQ3BCLG9DQUFpQixHQUFHLEVBQUUsQ0FBQztJQTBDbEMseUJBQUM7Q0FBQSxBQS9DRCxJQStDQztTQS9DWSxrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBWYWxpZGF0b3IsIEFic3RyYWN0Q29udHJvbCwgVmFsaWRhdGlvbkVycm9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbmV4cG9ydCBjbGFzcyBaZXVzUGhvbmVWYWxpZGF0b3IgaW1wbGVtZW50cyBWYWxpZGF0b3Ige1xyXG5cclxuICAgIHN0YXRpYyBwaG9uZUxlbmd0aCA9IDk7XHJcbiAgICBzdGF0aWMgY2VscGhvbmVMZW5ndGggPSAxMTtcclxuICAgIHN0YXRpYyBwaG9uZURERExlbmd0aCA9IDE0O1xyXG4gICAgc3RhdGljIGNlbHBob25lRERETGVuZ3RoID0gMTY7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBWYWxpZGEgdW0gQ1BGIG91IENOUEogZGUgYWNvcmRvIGNvbSBzZXUgZMOtZ2l0byB2ZXJpZmljYWRvci5cclxuICAgICAqL1xyXG4gICAgc3RhdGljIHZhbGlkYXRlKGM6IEFic3RyYWN0Q29udHJvbCk6IFZhbGlkYXRpb25FcnJvcnMgfCBudWxsIHtcclxuXHJcbiAgICAgICAgY29uc3QgcGhvbmUgPSBjLnZhbHVlO1xyXG5cclxuICAgICAgICAvLyBWZXJpZmljYSBvIHRhbWFuaG8gZGEgc3RyaW5nLlxyXG4gICAgICAgIGNvbnN0IGxlbmd0aEFycmF5ID0gW1xyXG4gICAgICAgICAgICBaZXVzUGhvbmVWYWxpZGF0b3IucGhvbmVMZW5ndGgsIFxyXG4gICAgICAgICAgICBaZXVzUGhvbmVWYWxpZGF0b3IuY2VscGhvbmVMZW5ndGgsIFxyXG4gICAgICAgICAgICBaZXVzUGhvbmVWYWxpZGF0b3IucGhvbmVERERMZW5ndGgsIFxyXG4gICAgICAgICAgICBaZXVzUGhvbmVWYWxpZGF0b3IuY2VscGhvbmVERERMZW5ndGhcclxuICAgICAgICBdO1xyXG4gICAgICAgIGlmIChsZW5ndGhBcnJheS5pbmRleE9mKHBob25lLmxlbmd0aCkgPCAwKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB7IGxlbmd0aDogdHJ1ZSB9O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgcmVnZXhBcnJheTogUmVnRXhwW10gPSBbXHJcbiAgICAgICAgICAgIC9eXFxkezR9LVxcZHs0fSQvLFxyXG4gICAgICAgICAgICAvXlxcZFxcc1xcZHs0fS1cXGR7NH0kLyxcclxuICAgICAgICAgICAgL15cXChcXGR7Mn1cXClcXHNcXGR7NH0tXFxkezR9JC8sXHJcbiAgICAgICAgICAgIC9eXFwoXFxkezJ9XFwpXFxzXFxkXFxzXFxkezR9LVxcZHs0fSQvXHJcbiAgICAgICAgXTtcclxuICAgICAgICBjb25zdCB2YWxpZCA9IHJlZ2V4QXJyYXkuc29tZShyZWdleCA9PiByZWdleC50ZXN0KHBob25lKSk7XHJcblxyXG4gICAgICAgIGlmICghdmFsaWQpIHtcclxuICAgICAgICAgICAgLy8gRMOtZ2l0byB2ZXJpZmljYWRvciBuw6NvIMOpIHbDoWxpZG8sIHJlc3VsdGFuZG8gZW0gZmFsaGEuXHJcbiAgICAgICAgICAgIHJldHVybiB7IGRpZ2l0OiB0cnVlIH07XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEltcGxlbWVudGEgYSBpbnRlcmZhY2UgZGUgdW0gdmFsaWRhdG9yLlxyXG4gICAgICovXHJcbiAgICB2YWxpZGF0ZShjOiBBYnN0cmFjdENvbnRyb2wpOiBWYWxpZGF0aW9uRXJyb3JzIHwgbnVsbCB7XHJcbiAgICAgICAgcmV0dXJuIFpldXNQaG9uZVZhbGlkYXRvci52YWxpZGF0ZShjKTtcclxuICAgIH1cclxufSJdfQ==