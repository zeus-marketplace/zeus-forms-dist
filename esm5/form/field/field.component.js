import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormBuilder } from '@angular/forms';
var FieldComponent = /** @class */ (function () {
    function FieldComponent(fb) {
        this.fb = fb;
        this.multiple = false;
        this.control = this.fb.control({
            item: [this.multiple ? [] : null]
        });
        this.complete = new EventEmitter();
    }
    FieldComponent.prototype.ngOnInit = function () {
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], FieldComponent.prototype, "type", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], FieldComponent.prototype, "placeholder", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], FieldComponent.prototype, "options", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], FieldComponent.prototype, "label", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], FieldComponent.prototype, "multiple", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", FormControl)
    ], FieldComponent.prototype, "control", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", Object)
    ], FieldComponent.prototype, "complete", void 0);
    FieldComponent = tslib_1.__decorate([
        Component({
            selector: 'zeus-field',
            template: "<ng-container [ngSwitch]=\"type\">\r\n\r\n\r\n  <ng-container *ngSwitchDefault>\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'textarea'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <textarea matInput [formControl]=\"control\" class=\"zeus-field\" rows=\"8\" [placeholder]=\"placeholder\"></textarea>\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'cpf'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" zeusCPF class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'cnpj'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" zeusCNPJ class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'cep'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" zeusCEP class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'telefone'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <input matInput [formControl]=\"control\" zeusTelefone class=\"zeus-field\" [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'select'\">\r\n    <mat-form-field class=\"zeus-form-field\">\r\n      <mat-select [formControl]=\"control\" class=\"zeus-field\" [class.multiple]=\"multiple\" [multiple]=\"multiple\"\r\n        [placeholder]=\"placeholder\">\r\n        <mat-option *ngFor=\"let option of options\" [value]=\"option.value\">\r\n          {{option.label}}\r\n        </mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'checkbox'\">\r\n    <div class=\"zeus-form-field\">\r\n      <mat-checkbox [formControl]=\"control\" class=\"zeus-checkbox\">\r\n        {{label}}\r\n      </mat-checkbox>\r\n    </div>\r\n  </ng-container>\r\n\r\n  <ng-container *ngSwitchCase=\"'radio'\">\r\n    <mat-label class=\"zeus-label\">{{label}}</mat-label>\r\n    <div class=\"zeus-form-field\">\r\n      <mat-radio-group [formControl]=\"control\" class=\"zeus-radio-group\">\r\n        <mat-radio-button class=\"zeus-radio-input\" *ngFor=\"let option of options\" [value]=\"option.value\">\r\n          {{option.label}}</mat-radio-button>\r\n      </mat-radio-group>\r\n    </div>\r\n\r\n  </ng-container>\r\n\r\n  <ng-container *ngIf=\"control?.errors && control?.touched\">\r\n\r\n    <ng-content select=\".zeus-error.zeus-error-required\" *ngIf=\"control.errors.required\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-invalid\" *ngIf=\"control.errors.pattern\"></ng-content>\r\n\r\n    <ng-content select=\".zeus-error.zeus-error-cpf-digit\" *ngIf=\"control.errors.digit\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-cpf-equal\" *ngIf=\"control.errors.equalDigits\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-cpf-length\" *ngIf=\"control.errors.length\"></ng-content>\r\n\r\n    <ng-content select=\".zeus-error.zeus-error-cnpj-digit\" *ngIf=\"control.errors.digit\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-cnpj-equal\" *ngIf=\"control.errors.equalDigits\"></ng-content>\r\n    <ng-content select=\".zeus-error.zeus-error-cnpj-length\" *ngIf=\"control.errors.length\"></ng-content>\r\n\r\n  </ng-container>\r\n\r\n  <ng-content select=\".zeus-hint\"></ng-content>\r\n\r\n\r\n</ng-container>",
            styles: [":host{display:block}:host .zeus-form-field{width:100%}"]
        }),
        tslib_1.__metadata("design:paramtypes", [FormBuilder])
    ], FieldComponent);
    return FieldComponent;
}());
export { FieldComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHpldXMvZm9ybXMvIiwic291cmNlcyI6WyJmb3JtL2ZpZWxkL2ZpZWxkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMvRSxPQUFPLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBTzFEO0lBY0Usd0JBQ1UsRUFBZTtRQUFmLE9BQUUsR0FBRixFQUFFLENBQWE7UUFUaEIsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixZQUFPLEdBQWdCLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO1lBQzlDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1NBQ2xDLENBQUMsQ0FBQztRQUVPLGFBQVEsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO0lBS3BDLENBQUM7SUFFTCxpQ0FBUSxHQUFSO0lBQ0EsQ0FBQztJQWpCUTtRQUFSLEtBQUssRUFBRTs7Z0RBQU07SUFDTDtRQUFSLEtBQUssRUFBRTs7dURBQWE7SUFDWjtRQUFSLEtBQUssRUFBRTs7bURBQVM7SUFDUjtRQUFSLEtBQUssRUFBRTs7aURBQU87SUFDTjtRQUFSLEtBQUssRUFBRTs7b0RBQWtCO0lBQ2pCO1FBQVIsS0FBSyxFQUFFOzBDQUFVLFdBQVc7bURBRTFCO0lBRU87UUFBVCxNQUFNLEVBQUU7O29EQUErQjtJQVg3QixjQUFjO1FBTDFCLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxZQUFZO1lBQ3RCLDh3SEFBcUM7O1NBRXRDLENBQUM7aURBZ0JjLFdBQVc7T0FmZCxjQUFjLENBcUIxQjtJQUFELHFCQUFDO0NBQUEsQUFyQkQsSUFxQkM7U0FyQlksY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUNvbnRyb2wsIEZvcm1CdWlsZGVyIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICd6ZXVzLWZpZWxkJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZmllbGQuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2ZpZWxkLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEZpZWxkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBcclxuICBASW5wdXQoKSB0eXBlO1xyXG4gIEBJbnB1dCgpIHBsYWNlaG9sZGVyO1xyXG4gIEBJbnB1dCgpIG9wdGlvbnM7XHJcbiAgQElucHV0KCkgbGFiZWw7XHJcbiAgQElucHV0KCkgbXVsdGlwbGUgPSBmYWxzZTsgXHJcbiAgQElucHV0KCkgY29udHJvbDogRm9ybUNvbnRyb2wgPSB0aGlzLmZiLmNvbnRyb2woe1xyXG4gICAgaXRlbTogW3RoaXMubXVsdGlwbGUgPyBbXSA6IG51bGxdXHJcbiAgfSk7XHJcblxyXG4gIEBPdXRwdXQoKSBjb21wbGV0ZSA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuIFxyXG4gIFxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBmYjogRm9ybUJ1aWxkZXJcclxuICApIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==