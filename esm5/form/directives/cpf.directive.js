import * as tslib_1 from "tslib";
import { Directive, HostListener, ElementRef, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { ZeusDocumentValidator } from '../validators/document.validator';
var ZeusCPFDirective = /** @class */ (function () {
    function ZeusCPFDirective(input) {
        this.input = input;
    }
    ZeusCPFDirective.prototype.ngOnInit = function () {
        this.formControl.setValidators(ZeusDocumentValidator.validate);
    };
    ZeusCPFDirective.prototype.onInput = function (event) {
        var value = event.target.value.replace(/[^0-9]+/g, '').slice(0, 11);
        value = value.replace(/([0-9]{3})([0-9])/, '$1.$2');
        value = value.replace(/\.([0-9]{3})([0-9])/, '.$1.$2');
        value = value.replace(/([0-9])([0-9]{1,2})$/, '$1-$2');
        this.formControl.setValue(value);
    };
    tslib_1.__decorate([
        Input('formControl'),
        tslib_1.__metadata("design:type", AbstractControl)
    ], ZeusCPFDirective.prototype, "formControl", void 0);
    tslib_1.__decorate([
        HostListener('input', ['$event']),
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", [Object]),
        tslib_1.__metadata("design:returntype", void 0)
    ], ZeusCPFDirective.prototype, "onInput", null);
    ZeusCPFDirective = tslib_1.__decorate([
        Directive({
            selector: '[zeusCPF]',
        }),
        tslib_1.__metadata("design:paramtypes", [ElementRef])
    ], ZeusCPFDirective);
    return ZeusCPFDirective;
}());
export { ZeusCPFDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3BmLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0B6ZXVzL2Zvcm1zLyIsInNvdXJjZXMiOlsiZm9ybS9kaXJlY3RpdmVzL2NwZi5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0UsT0FBTyxFQUEwQixlQUFlLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN6RSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUt6RTtJQUdJLDBCQUFvQixLQUFpQjtRQUFqQixVQUFLLEdBQUwsS0FBSyxDQUFZO0lBQUUsQ0FBQztJQUV4QyxtQ0FBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDbkUsQ0FBQztJQUdELGtDQUFPLEdBQVAsVUFBUSxLQUFLO1FBRVQsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ3BFLEtBQUssR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLG1CQUFtQixFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ3BELEtBQUssR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLHFCQUFxQixFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZELEtBQUssR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLHNCQUFzQixFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBRXZELElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBRXJDLENBQUM7SUFqQnFCO1FBQXJCLEtBQUssQ0FBQyxhQUFhLENBQUM7MENBQWMsZUFBZTt5REFBQztJQVFuRDtRQURDLFlBQVksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQzs7OzttREFVakM7SUFuQlEsZ0JBQWdCO1FBSDVCLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxXQUFXO1NBQ3hCLENBQUM7aURBSTZCLFVBQVU7T0FINUIsZ0JBQWdCLENBcUI1QjtJQUFELHVCQUFDO0NBQUEsQUFyQkQsSUFxQkM7U0FyQlksZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBIb3N0TGlzdGVuZXIsIEVsZW1lbnRSZWYsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5nQ29udHJvbCwgRm9ybUNvbnRyb2wsIEFic3RyYWN0Q29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgWmV1c0RvY3VtZW50VmFsaWRhdG9yIH0gZnJvbSAnLi4vdmFsaWRhdG9ycy9kb2N1bWVudC52YWxpZGF0b3InO1xyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1t6ZXVzQ1BGXScsXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBaZXVzQ1BGRGlyZWN0aXZlIHtcclxuXHJcbiAgICBASW5wdXQoJ2Zvcm1Db250cm9sJykgZm9ybUNvbnRyb2w6IEFic3RyYWN0Q29udHJvbDtcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaW5wdXQ6IEVsZW1lbnRSZWYpe31cclxuICAgIFxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5mb3JtQ29udHJvbC5zZXRWYWxpZGF0b3JzKFpldXNEb2N1bWVudFZhbGlkYXRvci52YWxpZGF0ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignaW5wdXQnLCBbJyRldmVudCddKVxyXG4gICAgb25JbnB1dChldmVudCl7XHJcbiAgICAgICAgXHJcbiAgICAgICAgbGV0IHZhbHVlID0gZXZlbnQudGFyZ2V0LnZhbHVlLnJlcGxhY2UoL1teMC05XSsvZywgJycpLnNsaWNlKDAsIDExKTtcclxuICAgICAgICB2YWx1ZSA9IHZhbHVlLnJlcGxhY2UoLyhbMC05XXszfSkoWzAtOV0pLywgJyQxLiQyJyk7XHJcbiAgICAgICAgdmFsdWUgPSB2YWx1ZS5yZXBsYWNlKC9cXC4oWzAtOV17M30pKFswLTldKS8sICcuJDEuJDInKTtcclxuICAgICAgICB2YWx1ZSA9IHZhbHVlLnJlcGxhY2UoLyhbMC05XSkoWzAtOV17MSwyfSkkLywgJyQxLSQyJyk7XHJcblxyXG4gICAgICAgIHRoaXMuZm9ybUNvbnRyb2wuc2V0VmFsdWUodmFsdWUpO1xyXG4gICAgICAgIFxyXG4gICAgfVxyXG4gICAgXHJcbn0iXX0=