import * as tslib_1 from "tslib";
import { Directive, HostListener, ElementRef, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { FieldComponent } from '../field/field.component';
import { ZeusPhoneValidator } from '../validators/phone.validator';
var ZeusTelefoneDirective = /** @class */ (function () {
    function ZeusTelefoneDirective(input, host) {
        this.input = input;
        this.host = host;
    }
    ZeusTelefoneDirective.prototype.ngOnInit = function () {
        this.formControl.setValidators(ZeusPhoneValidator.validate);
    };
    ZeusTelefoneDirective.prototype.onInput = function (event) {
        var value = event.target.value.replace(/[^0-9]+/g, '').slice(0, 11);
        this.host.complete.emit({
            telefone: value
        });
        if (value.length <= 8) {
            value = value.replace(/^(\d{4})(\d)/, '$1-$2');
        }
        else if (value.length <= 9) {
            value = value.replace(/^(\d)(\d)/, '$1 $2');
            value = value.replace(/(\d)(\d{4})$/, '$1-$2');
        }
        else if (value.length <= 10) {
            value = value.replace(/^(\d{2})(\d)/, '($1) $2');
            value = value.replace(/(\d)(\d{4})$/, '$1-$2');
        }
        else {
            value = value.replace(/^(\d{2})(\d)/, '($1) $2');
            value = value.replace(/(\d)(\d{8})$/, '$1 $2');
            value = value.replace(/(\d)(\d{4})$/, '$1-$2');
        }
        this.formControl.setValue(value);
    };
    tslib_1.__decorate([
        Input('formControl'),
        tslib_1.__metadata("design:type", AbstractControl)
    ], ZeusTelefoneDirective.prototype, "formControl", void 0);
    tslib_1.__decorate([
        HostListener('input', ['$event']),
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", [Object]),
        tslib_1.__metadata("design:returntype", void 0)
    ], ZeusTelefoneDirective.prototype, "onInput", null);
    ZeusTelefoneDirective = tslib_1.__decorate([
        Directive({
            selector: '[zeusTelefone]',
        }),
        tslib_1.__metadata("design:paramtypes", [ElementRef, FieldComponent])
    ], ZeusTelefoneDirective);
    return ZeusTelefoneDirective;
}());
export { ZeusTelefoneDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVsZWZvbmUuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHpldXMvZm9ybXMvIiwic291cmNlcyI6WyJmb3JtL2RpcmVjdGl2ZXMvdGVsZWZvbmUuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDMUQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFLbkU7SUFHSSwrQkFBb0IsS0FBaUIsRUFBVSxJQUFvQjtRQUEvQyxVQUFLLEdBQUwsS0FBSyxDQUFZO1FBQVUsU0FBSSxHQUFKLElBQUksQ0FBZ0I7SUFBSSxDQUFDO0lBRXhFLHdDQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNoRSxDQUFDO0lBR0QsdUNBQU8sR0FBUCxVQUFRLEtBQUs7UUFFVCxJQUFJLEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO1lBQ3BCLFFBQVEsRUFBRSxLQUFLO1NBQ2xCLENBQUMsQ0FBQztRQUVILElBQUksS0FBSyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7WUFDbkIsS0FBSyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1NBQ2xEO2FBQU0sSUFBSSxLQUFLLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtZQUMxQixLQUFLLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDNUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1NBQ2xEO2FBQU0sSUFBSSxLQUFLLENBQUMsTUFBTSxJQUFJLEVBQUUsRUFBRTtZQUMzQixLQUFLLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsU0FBUyxDQUFDLENBQUM7WUFDakQsS0FBSyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1NBQ2xEO2FBQU07WUFDSCxLQUFLLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsU0FBUyxDQUFDLENBQUM7WUFDakQsS0FBSyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQy9DLEtBQUssR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxPQUFPLENBQUMsQ0FBQztTQUNsRDtRQUVELElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBRXJDLENBQUM7SUEvQnFCO1FBQXJCLEtBQUssQ0FBQyxhQUFhLENBQUM7MENBQWMsZUFBZTs4REFBQztJQVFuRDtRQURDLFlBQVksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQzs7Ozt3REF3QmpDO0lBakNRLHFCQUFxQjtRQUhqQyxTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsZ0JBQWdCO1NBQzdCLENBQUM7aURBSTZCLFVBQVUsRUFBZ0IsY0FBYztPQUgxRCxxQkFBcUIsQ0FtQ2pDO0lBQUQsNEJBQUM7Q0FBQSxBQW5DRCxJQW1DQztTQW5DWSxxQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEhvc3RMaXN0ZW5lciwgRWxlbWVudFJlZiwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWJzdHJhY3RDb250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBGaWVsZENvbXBvbmVudCB9IGZyb20gJy4uL2ZpZWxkL2ZpZWxkLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFpldXNQaG9uZVZhbGlkYXRvciB9IGZyb20gJy4uL3ZhbGlkYXRvcnMvcGhvbmUudmFsaWRhdG9yJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICdbemV1c1RlbGVmb25lXScsXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBaZXVzVGVsZWZvbmVEaXJlY3RpdmUge1xyXG5cclxuICAgIEBJbnB1dCgnZm9ybUNvbnRyb2wnKSBmb3JtQ29udHJvbDogQWJzdHJhY3RDb250cm9sO1xyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBpbnB1dDogRWxlbWVudFJlZiwgcHJpdmF0ZSBob3N0OiBGaWVsZENvbXBvbmVudCkgeyB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5mb3JtQ29udHJvbC5zZXRWYWxpZGF0b3JzKFpldXNQaG9uZVZhbGlkYXRvci52YWxpZGF0ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignaW5wdXQnLCBbJyRldmVudCddKVxyXG4gICAgb25JbnB1dChldmVudCkge1xyXG5cclxuICAgICAgICBsZXQgdmFsdWUgPSBldmVudC50YXJnZXQudmFsdWUucmVwbGFjZSgvW14wLTldKy9nLCAnJykuc2xpY2UoMCwgMTEpO1xyXG4gICAgICAgIHRoaXMuaG9zdC5jb21wbGV0ZS5lbWl0KHtcclxuICAgICAgICAgICAgdGVsZWZvbmU6IHZhbHVlXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGlmICh2YWx1ZS5sZW5ndGggPD0gOCkge1xyXG4gICAgICAgICAgICB2YWx1ZSA9IHZhbHVlLnJlcGxhY2UoL14oXFxkezR9KShcXGQpLywgJyQxLSQyJyk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh2YWx1ZS5sZW5ndGggPD0gOSkge1xyXG4gICAgICAgICAgICB2YWx1ZSA9IHZhbHVlLnJlcGxhY2UoL14oXFxkKShcXGQpLywgJyQxICQyJyk7XHJcbiAgICAgICAgICAgIHZhbHVlID0gdmFsdWUucmVwbGFjZSgvKFxcZCkoXFxkezR9KSQvLCAnJDEtJDInKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHZhbHVlLmxlbmd0aCA8PSAxMCkge1xyXG4gICAgICAgICAgICB2YWx1ZSA9IHZhbHVlLnJlcGxhY2UoL14oXFxkezJ9KShcXGQpLywgJygkMSkgJDInKTtcclxuICAgICAgICAgICAgdmFsdWUgPSB2YWx1ZS5yZXBsYWNlKC8oXFxkKShcXGR7NH0pJC8sICckMS0kMicpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHZhbHVlID0gdmFsdWUucmVwbGFjZSgvXihcXGR7Mn0pKFxcZCkvLCAnKCQxKSAkMicpO1xyXG4gICAgICAgICAgICB2YWx1ZSA9IHZhbHVlLnJlcGxhY2UoLyhcXGQpKFxcZHs4fSkkLywgJyQxICQyJyk7XHJcbiAgICAgICAgICAgIHZhbHVlID0gdmFsdWUucmVwbGFjZSgvKFxcZCkoXFxkezR9KSQvLCAnJDEtJDInKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuZm9ybUNvbnRyb2wuc2V0VmFsdWUodmFsdWUpO1xyXG5cclxuICAgIH1cclxuXHJcbn0iXX0=