import * as tslib_1 from "tslib";
import { Directive, HostListener, ElementRef, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { FieldComponent } from '../field/field.component';
var ZeusCEPDirective = /** @class */ (function () {
    function ZeusCEPDirective(input, host) {
        this.input = input;
        this.host = host;
    }
    ZeusCEPDirective.prototype.ngOnInit = function () {
        console.log(this.host);
    };
    ZeusCEPDirective.prototype.onInput = function (event) {
        var value = event.target.value.replace(/[^0-9]+/g, '').slice(0, 8);
        this.host.complete.emit({
            cep: value
        });
        value = value.replace(/^(\d{2})(\d)/, '$1.$2'); //Coloca ponto entre o segundo e o terceiro dígitos
        value = value.replace(/(\d)(\d{3})$/, '$1-$2'); //Coloca ponto entre o quinto e o sexto dígitos
        this.formControl.setValue(value);
    };
    tslib_1.__decorate([
        Input('formControl'),
        tslib_1.__metadata("design:type", AbstractControl)
    ], ZeusCEPDirective.prototype, "formControl", void 0);
    tslib_1.__decorate([
        HostListener('input', ['$event']),
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", [Object]),
        tslib_1.__metadata("design:returntype", void 0)
    ], ZeusCEPDirective.prototype, "onInput", null);
    ZeusCEPDirective = tslib_1.__decorate([
        Directive({
            selector: '[zeusCEP]',
        }),
        tslib_1.__metadata("design:paramtypes", [ElementRef, FieldComponent])
    ], ZeusCEPDirective);
    return ZeusCEPDirective;
}());
export { ZeusCEPDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2VwLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0B6ZXVzL2Zvcm1zLyIsInNvdXJjZXMiOlsiZm9ybS9kaXJlY3RpdmVzL2NlcC5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0UsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ2pELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUsxRDtJQUdJLDBCQUFvQixLQUFpQixFQUFVLElBQW9CO1FBQS9DLFVBQUssR0FBTCxLQUFLLENBQVk7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFnQjtJQUFJLENBQUM7SUFFeEUsbUNBQVEsR0FBUjtRQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFHRCxrQ0FBTyxHQUFQLFVBQVEsS0FBSztRQUVULElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7WUFDcEIsR0FBRyxFQUFFLEtBQUs7U0FDYixDQUFDLENBQUM7UUFFSCxLQUFLLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxtREFBbUQ7UUFDbkcsS0FBSyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsK0NBQStDO1FBRS9GLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBRXJDLENBQUM7SUFwQnFCO1FBQXJCLEtBQUssQ0FBQyxhQUFhLENBQUM7MENBQWMsZUFBZTt5REFBQztJQVFuRDtRQURDLFlBQVksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQzs7OzttREFhakM7SUF0QlEsZ0JBQWdCO1FBSDVCLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxXQUFXO1NBQ3hCLENBQUM7aURBSTZCLFVBQVUsRUFBZ0IsY0FBYztPQUgxRCxnQkFBZ0IsQ0F3QjVCO0lBQUQsdUJBQUM7Q0FBQSxBQXhCRCxJQXdCQztTQXhCWSxnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEhvc3RMaXN0ZW5lciwgRWxlbWVudFJlZiwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWJzdHJhY3RDb250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBGaWVsZENvbXBvbmVudCB9IGZyb20gJy4uL2ZpZWxkL2ZpZWxkLmNvbXBvbmVudCc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW3pldXNDRVBdJyxcclxufSlcclxuZXhwb3J0IGNsYXNzIFpldXNDRVBEaXJlY3RpdmUge1xyXG5cclxuICAgIEBJbnB1dCgnZm9ybUNvbnRyb2wnKSBmb3JtQ29udHJvbDogQWJzdHJhY3RDb250cm9sO1xyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBpbnB1dDogRWxlbWVudFJlZiwgcHJpdmF0ZSBob3N0OiBGaWVsZENvbXBvbmVudCkgeyB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgY29uc29sZS5sb2codGhpcy5ob3N0KTtcclxuICAgIH1cclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCdpbnB1dCcsIFsnJGV2ZW50J10pXHJcbiAgICBvbklucHV0KGV2ZW50KSB7XHJcblxyXG4gICAgICAgIGxldCB2YWx1ZSA9IGV2ZW50LnRhcmdldC52YWx1ZS5yZXBsYWNlKC9bXjAtOV0rL2csICcnKS5zbGljZSgwLCA4KTtcclxuICAgICAgICB0aGlzLmhvc3QuY29tcGxldGUuZW1pdCh7XHJcbiAgICAgICAgICAgIGNlcDogdmFsdWVcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdmFsdWUgPSB2YWx1ZS5yZXBsYWNlKC9eKFxcZHsyfSkoXFxkKS8sICckMS4kMicpOyAvL0NvbG9jYSBwb250byBlbnRyZSBvIHNlZ3VuZG8gZSBvIHRlcmNlaXJvIGTDrWdpdG9zXHJcbiAgICAgICAgdmFsdWUgPSB2YWx1ZS5yZXBsYWNlKC8oXFxkKShcXGR7M30pJC8sICckMS0kMicpOyAvL0NvbG9jYSBwb250byBlbnRyZSBvIHF1aW50byBlIG8gc2V4dG8gZMOtZ2l0b3NcclxuXHJcbiAgICAgICAgdGhpcy5mb3JtQ29udHJvbC5zZXRWYWx1ZSh2YWx1ZSk7XHJcblxyXG4gICAgfVxyXG5cclxufSJdfQ==