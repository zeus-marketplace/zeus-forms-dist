import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatInputModule, MatSelectModule, MatButtonModule, MatOptionModule, MatCheckboxModule, MatButtonToggleModule, MatDatepickerModule, MatFormFieldModule, MatCardModule, MatRadioModule } from '@angular/material';
var modules = [
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatOptionModule,
    MatCheckboxModule,
    MatRadioModule,
    MatButtonToggleModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatCardModule
];
var ZeusMaterialModule = /** @class */ (function () {
    function ZeusMaterialModule() {
    }
    ZeusMaterialModule = tslib_1.__decorate([
        NgModule({
            declarations: [],
            imports: tslib_1.__spread([
                CommonModule
            ], modules),
            exports: tslib_1.__spread(modules)
        })
    ], ZeusMaterialModule);
    return ZeusMaterialModule;
}());
export { ZeusMaterialModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0ZXJpYWwubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHpldXMvZm9ybXMvIiwic291cmNlcyI6WyJtYXRlcmlhbC9tYXRlcmlhbC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFDTCxjQUFjLEVBQ2QsZUFBZSxFQUNmLGVBQWUsRUFDZixlQUFlLEVBQ2YsaUJBQWlCLEVBQ2pCLHFCQUFxQixFQUNyQixtQkFBbUIsRUFDbkIsa0JBQWtCLEVBQ2xCLGFBQWEsRUFDYixjQUFjLEVBQ2YsTUFBTSxtQkFBbUIsQ0FBQztBQUUzQixJQUFNLE9BQU8sR0FBRztJQUNkLGNBQWM7SUFDZCxlQUFlO0lBQ2YsZUFBZTtJQUNmLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsY0FBYztJQUNkLHFCQUFxQjtJQUNyQixtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLGFBQWE7Q0FDZCxDQUFBO0FBWUQ7SUFBQTtJQUFrQyxDQUFDO0lBQXRCLGtCQUFrQjtRQVY5QixRQUFRLENBQUM7WUFDUixZQUFZLEVBQUUsRUFBRTtZQUNoQixPQUFPO2dCQUNMLFlBQVk7ZUFDVCxPQUFPLENBQ1g7WUFDRCxPQUFPLG1CQUNGLE9BQU8sQ0FDWDtTQUNGLENBQUM7T0FDVyxrQkFBa0IsQ0FBSTtJQUFELHlCQUFDO0NBQUEsQUFBbkMsSUFBbUM7U0FBdEIsa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHtcclxuICBNYXRJbnB1dE1vZHVsZSxcclxuICBNYXRTZWxlY3RNb2R1bGUsXHJcbiAgTWF0QnV0dG9uTW9kdWxlLFxyXG4gIE1hdE9wdGlvbk1vZHVsZSxcclxuICBNYXRDaGVja2JveE1vZHVsZSxcclxuICBNYXRCdXR0b25Ub2dnbGVNb2R1bGUsXHJcbiAgTWF0RGF0ZXBpY2tlck1vZHVsZSxcclxuICBNYXRGb3JtRmllbGRNb2R1bGUsXHJcbiAgTWF0Q2FyZE1vZHVsZSxcclxuICBNYXRSYWRpb01vZHVsZVxyXG59IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuXHJcbmNvbnN0IG1vZHVsZXMgPSBbXHJcbiAgTWF0SW5wdXRNb2R1bGUsXHJcbiAgTWF0U2VsZWN0TW9kdWxlLFxyXG4gIE1hdEJ1dHRvbk1vZHVsZSxcclxuICBNYXRPcHRpb25Nb2R1bGUsXHJcbiAgTWF0Q2hlY2tib3hNb2R1bGUsXHJcbiAgTWF0UmFkaW9Nb2R1bGUsXHJcbiAgTWF0QnV0dG9uVG9nZ2xlTW9kdWxlLFxyXG4gIE1hdERhdGVwaWNrZXJNb2R1bGUsXHJcbiAgTWF0Rm9ybUZpZWxkTW9kdWxlLFxyXG4gIE1hdENhcmRNb2R1bGVcclxuXVxyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIENvbW1vbk1vZHVsZSxcclxuICAgIC4uLm1vZHVsZXNcclxuICBdLFxyXG4gIGV4cG9ydHM6IFtcclxuICAgIC4uLm1vZHVsZXNcclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBaZXVzTWF0ZXJpYWxNb2R1bGUgeyB9XHJcbiJdfQ==