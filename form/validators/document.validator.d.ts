import { Validator, AbstractControl, ValidationErrors } from '@angular/forms';
declare class ZeusDocumentValidator implements Validator {
    static cpfLength: number;
    static cnpjLength: number;
    /**
     * Calcula o dígito verificador do CPF ou CNPJ.
     */
    static buildDigit(arr: number[]): number;
    /**
     * Valida um CPF ou CNPJ de acordo com seu dígito verificador.
     */
    static validate(c: AbstractControl): ValidationErrors | null;
    /**
     * Implementa a interface de um validator.
     */
    validate(c: AbstractControl): ValidationErrors | null;
}
declare class ZeusDocumentDirective extends ZeusDocumentValidator implements Validator {
}
export { ZeusDocumentDirective, ZeusDocumentValidator };
