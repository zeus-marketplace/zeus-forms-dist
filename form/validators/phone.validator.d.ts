import { Validator, AbstractControl, ValidationErrors } from '@angular/forms';
export declare class ZeusPhoneValidator implements Validator {
    static phoneLength: number;
    static celphoneLength: number;
    static phoneDDDLength: number;
    static celphoneDDDLength: number;
    /**
     * Valida um CPF ou CNPJ de acordo com seu dígito verificador.
     */
    static validate(c: AbstractControl): ValidationErrors | null;
    /**
     * Implementa a interface de um validator.
     */
    validate(c: AbstractControl): ValidationErrors | null;
}
