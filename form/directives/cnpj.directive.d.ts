import { ElementRef } from '@angular/core';
import { AbstractControl } from '@angular/forms';
export declare class ZeusCNPJDirective {
    private input;
    formControl: AbstractControl;
    constructor(input: ElementRef);
    ngOnInit(): void;
    onInput(event: any): void;
}
