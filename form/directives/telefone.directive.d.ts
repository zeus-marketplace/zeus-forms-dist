import { ElementRef } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { FieldComponent } from '../field/field.component';
export declare class ZeusTelefoneDirective {
    private input;
    private host;
    formControl: AbstractControl;
    constructor(input: ElementRef, host: FieldComponent);
    ngOnInit(): void;
    onInput(event: any): void;
}
