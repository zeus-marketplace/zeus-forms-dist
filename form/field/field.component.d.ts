import { OnInit, EventEmitter } from '@angular/core';
import { FormControl, FormBuilder } from '@angular/forms';
export declare class FieldComponent implements OnInit {
    private fb;
    type: any;
    placeholder: any;
    options: any;
    label: any;
    multiple: boolean;
    control: FormControl;
    complete: EventEmitter<{}>;
    constructor(fb: FormBuilder);
    ngOnInit(): void;
}
